<?php

    define('Vitoco', true);
    $titre = 'Mon compte';
    $cache = false;

    require_once('includes/init.php');
    require_once('includes/head.php');
    require_once('includes/popup.php');

    $tpl = new Smarty;

    // Si on est connecté
    if ($_SESSION['visiteur']->estConnecte()) {

      // Traitements des formulaires
      if ($_SERVER['REQUEST_METHOD'] == "POST") {

        // Formulaires de suppression de véhicules
        if (!empty($_POST['drop-vehicule'])) {
          try {
            foreach ($_POST['drop-vehicule'] as $idVehicule) {
              $vehiculeManager = new VehiculeManager($bdd);
              $vehicule = $vehiculeManager->getVehicule(['idVehicule', DB::EGAL, $idVehicule]);
              $vehicule->setIdPossesseur(NULL);
              $vehiculeManager->update($vehicule);
            }
          }
          catch (Exception $e) {
            $_SESSION['popup_type'] = "error";
            $_SESSION['popup_content'] = "Une erreur est survenue lors de la suppresion de ce véhicule. Veuillez réessayer. ".$e->getMessage();
            header('Location: account.php?t=vehicles');
          }
        }

        // Formulaire d'ajout de véhicule
        if (!empty($_POST['marque'])) {
          try {
            $vehicule = new Vehicule();
            $vehicule->setMarque($_POST['marque']);
            $vehicule->setModele($_POST['modele']);
            $vehicule->setNbrPlacesPassagers($_POST['nbrPlacesPassagers']);
            $vehicule->setIdPossesseur($_SESSION['visiteur']->getIdMembre());
            if (!empty($_POST['annee'])) $vehicule->setAnnee($_POST['annee']);
            if (!empty($_POST['consommation'])) $vehicule->setConsommation($_POST['consommation']);
            $vehiculeManager = new VehiculeManager($bdd);
            $vehiculeManager->insert($vehicule);
          }
          catch (Exception $e) {
            $_SESSION['popup_type'] = "error";
            $_SESSION['popup_content'] = "Une erreur est survenue lors de l'ajout de ce véhicule. Veuillez réessayer. ".$e->getMessage();
            header('Location: account.php?t=vehicles');
          }
        }

        // Formulaire de modifications des informations
        if (!empty($_POST['prenom']))
        {
          try {
            $membreManager = new MembreManager($bdd);
            $membre = $membreManager->getMembre(['idMembre', DB::EGAL, $_SESSION['visiteur']->getIdMembreConnecte()]);
            $adresseManager = new AdresseManager($bdd);
            $membreManager->loadAdresses([$membre]);
            $adresse = $membre->getAdresse();

            $membre->setPrenom($_POST['prenom']);
            $membre->setNom($_POST['nom']);
            $membre->setDateNaissance($_POST['naissance']);
            $membre->setTelephone($_POST['tel']);
            $adresse->setNumeroVoie($_POST['num-voie']);
            $adresse->setNomVoie($_POST['nom-voie']);
            $adresse->setComplement($_POST['complement']);
            $adresse->setCP($_POST['cp']);
            $adresse->setVille($_POST['ville']);
            if (!empty($_POST['pwd'])) {
              if ($_POST['pwd'] != $_POST['pwd-check']) throw new Exception("Les mots de passe ne correspondent pas.");
              else $membre->setMdp($_POST['pwd'], true);
            }

            $adresseManager->update($adresse);
            $membreManager->update($membre);
          }
          catch (Exception $e) {
            $_SESSION['popup_type'] = "error";
            $_SESSION['popup_content'] = "Une erreur est survenue lors de la modification de vos informations. Veuillez réessayer. ".$e->getMessage();
            header('Location: account.php?t=infos');
          }
        }
      }

      // Affichage des pages
      if (!empty($_GET['t']) && in_array($_GET['t'], ["infos", "vehicles", "list"])) {

        $membreManager = new MembreManager($bdd);
        $membre = $membreManager->getMembre(['idMembre', DB::EGAL, $_SESSION['visiteur']->getIdMembreConnecte()]);
        $tpl->assign(array(
          'membre' => $membre
          ));

        switch ($_GET['t']) {

          case "infos":
            $membreManager->loadAdresses([$membre]);
            $adresse = $membre->getAdresse();
            $tpl->assign(array(
              'dateMinus18' => ((new DateTime("now"))->sub(new DateInterval("P18Y")))->format("Y-m-d"),
              'ancienPrenom' => $membre->getPrenom(),
              'ancienNom' => $membre->getNom(),
              'ancienNaissance' => $membre->getDateNaissance("Y-m-d"),
              'ancienTel' => $membre->getTelephone(),
              'ancienNumVoie' => $adresse->getNumeroVoie(),
              'ancienNomVoie' => $adresse->getNomVoie(),
              'ancienComplement' => $adresse->getComplement(),
              'ancienCP' => $adresse->getCP(),
              'ancienVille' => $adresse->getVille(),
              'ancienMail' => $membre->getMail()
          		));
            $tpl->display('specific/account-info.html');
            break;

          case "vehicles":
            $membreManager->loadVehicules([$membre]);
            $tpl->assign(array(
              'vehicules' => $membre->getVehicules()
          		));
            $tpl->display('specific/account-vehicles.html');
            break;

          case "list":
            $trajetDisponibleManager = new TrajetDisponibleManager($bdd);
            $estConducteurDans = $trajetDisponibleManager->getListe("", ['conducteur', DB::EGAL, $membre->getIdMembre()]);
            $estParticipantDans = $trajetDisponibleManager->getTrajetsDuParticipant($membre->getIdMembre());
            $tpl->assign(array(
              'estConducteurDans' => $estConducteurDans,
              'estParticipantDans' => $estParticipantDans
          		));
            $tpl->display('specific/account-list.html');
        }
      }

      // Si on accède à la mauvaise page
      else {
        $_SESSION['popup_type'] = "error";
        $_SESSION['popup_content'] = "Impossible d'accéder à cette page, son adresse est mal formée.";
        header('Location: index.php');
      }
    }

    // Si on est pas connecté
    else {
      $_SESSION['popup_type'] = "error";
      $_SESSION['popup_content'] = "Vous devez être connecté pour pouvoir accéder à cette page.";
      header('Location: index.php');
    }

    require_once('includes/footer.php');
