<?php

    define('Vitoco', true);
    $titre = 'Vitoco - Soumettre un trajet';
    $cache = false;

    require_once('includes/init.php');
    require_once('includes/head.php');
    require_once('includes/popup.php');

    $tpl = new Smarty;

    // Traitement des formulaires
    if ($_SERVER['REQUEST_METHOD'] == "POST" && $_SESSION['visiteur']->estConnecte()) {

      $dateJour = new DateTime("now");
      $tpl->assign(array(
        'dateBonus1H' => ($dateJour->add(new DateInterval("PT1H")))->format("Y-m-d"),
        'dateBonus1Y' => ($dateJour->add(new DateInterval("P1Y")))->format("Y-m-d")
      	));

      // Affichage du formulaire de création de trajet après avoir choisi le type
      if (!empty($_POST['tt'])) {
          $membreManager = new MembreManager($bdd);
          $membre = $membreManager->getMembre(['idMembre', DB::EGAL, $_SESSION['visiteur']->getIdMembre()]);
          $membreManager->loadVehicules([$membre]);
          $vehicules = $membre->getVehicules();
        if ($_POST['tt'] == "custom") {
          $tpl->assign(array(
              'estTrajetCustom' => true,
              'premierVehicule' => array_shift($vehicules),
              'autresVehicules' => $vehicules
        		));
        }
        else {
          $trajetTypeManager = new TrajetTypeManager($bdd);
          $trajetType = $trajetTypeManager->getTrajetType(['idTrajetType', DB::EGAL, intval($_POST['tt'])]);

          $tpl->assign(array(
            'estTrajetCustom' => false,
            'plafondPrixKm' => $trajetType->getPlafondPrixKm(),
            'distance' => $trajetType->getDistance(),
            'temps' => $trajetType->getTempsIndicatif(),
            'villeDepart' => $trajetType->getVilleDepart()->getVille(),
            'villeArrivee' => $trajetType->getVilleArrivee()->getVille(),
            'premierVehicule' => array_shift($vehicules),
            'autresVehicules' => $vehicules
        		));
        }

        $tpl->display('specific/submit.html');
      }

      // Traitement du formulaire de création de trajet
      else if (isset($_POST['datetime']) && $_SESSION['visiteur']->estConnecte()) {
        try {
          $trajetDisponibleManager = new TrajetDisponibleManager($bdd);
          $adresseManager = new AdresseManager($bdd);

          $trajet = new TrajetDisponible;
          $adresseDepart = new Adresse;
          if (!empty($_POST['depart-num-voie'])) $adresseDepart->setNumeroVoie($_POST['depart-num-voie']);
          if (!empty($_POST['depart-nom-voie'])) $adresseDepart->setNomVoie($_POST['depart-nom-voie']);
          if (!empty($_POST['depart-cp'])) $adresseDepart->setCP($_POST['depart-cp']);
          $adresseDepart->setVille($_POST['depart-ville']);
          if (!empty($_POST['depart-complement'])) $adresseDepart->setComplement($_POST['depart-complement']);
          $adresseDepart = $adresseManager->getAdresse(['idAdresse', DB::EGAL, $adresseManager->insert($adresseDepart)]);
          $trajet->setVilleDepart($adresseDepart);

          $i = 1;
          while (!empty($_POST['e'.$i.'-ville'])) {
            $etape = new Adresse;
            if (!empty($_POST['e'.$i.'-num-voie'])) $etape->setNumeroVoie($_POST['e'.$i.'-num-voie']);
            if (!empty($_POST['e'.$i.'-nom-voie'])) $etape->setNomVoie($_POST['e'.$i.'-nom-voie']);
            if (!empty($_POST['e'.$i.'-cp'])) $etape->setCP($_POST['e'.$i.'-cp']);
            $etape->setVille($_POST['e'.$i.'-ville']);
            if (!empty($_POST['e'.$i.'-complement'])) $etape->setComplement($_POST['e'.$i.'-complement']);
            $etape = $adresseManager->getAdresse(['idAdresse', DB::EGAL, $adresseManager->insert($etape)]);
            $trajet->addEtape($etape);
            $i++;
          }

          $adresseArrive = new Adresse;
          if (!empty($_POST['arrive-num-voie'])) $adresseArrive->setNumeroVoie($_POST['arrive-num-voie']);
          if (!empty($_POST['arrive-nom-voie'])) $adresseArrive->setNomVoie($_POST['arrive-nom-voie']);
          if (!empty($_POST['arrive-cp'])) $adresseArrive->setCP($_POST['arrive-cp']);
          $adresseArrive->setVille($_POST['arrive-ville']);
          if (!empty($_POST['arrive-complement'])) $adresseArrive->setComplement($_POST['arrive-complement']);
          $adresseArrive = $adresseManager->getAdresse(['idAdresse', DB::EGAL, $adresseManager->insert($adresseArrive)]);
          $trajet->setVilleArrivee($adresseArrive);

          $trajet->setDistance($_POST['distance']);
          $trajet->setDureeEstimee($_POST['temps']);
          if (!$trajet->setDateTrajet($_POST['datetime']))
          {
              throw new Exception("La date du trajet n'est pas valide !");
          }
          $trajet->setPrixKm($_POST['prix-km']);
          if ($_POST['fumer'] == "oui")
              $trajet->setCigaretteAutorisee(true);
          $trajet->setBagagesAutorises($_POST['bagage']);
          if ($_POST['animal'] == "oui")
            $trajet->setAnimalAutorise(true);
          if (!empty($_POST['details'])) $trajet->setDescription($_POST['details']);

          $membreManager = new MembreManager($bdd);
          $membre = $_SESSION['visiteur'];
          $membreManager->loadVehicules([$membre]);

          $trajet->setConducteur($membre->getIdMembre());
          $trajet->setDateCreation('now');
          $trajet->setVehicule($_POST['vehicule']);

          $trajet = $trajetDisponibleManager->insert($trajet);

          $_SESSION['popup_type'] = "notification";
          $_SESSION['popup_content'] = "Votre trajet a bien été créé.";
          header('Location: trip.php?t='.$trajet);
        }
        catch (Exception $e) {
          $_SESSION['popup_type'] = "error";
          $_SESSION['popup_content'] = "Une erreur est survenue lors de la création de votre trajet. Veuillez réessayer.".$e->getMessage();
          header('Location: submit.php');
        }
      }
    }

    // Afficher le choix du type d'itinéraire
    else {

      // Si le visiteur est connecté
      if ($_SESSION['visiteur']->estConnecte()) {
        $membreManager = new MembreManager($bdd);
        $membreManager->loadVehicules([$_SESSION['visiteur']]);

        // Si le membre n'a enregistré aucun véhicule
        if (empty($_SESSION['visiteur']->getVehicules())) {
          $_SESSION['popup_type'] = "error";
          $_SESSION['popup_content'] = "Vous devez avoir renseigné un véhicule dans votre compte pour pouvoir accéder à cette page.<br>";
          header('Location: index.php');
        }

        // Si le membre a enregistré au moins un véhicule
        else {
          $trajetTypeManager = new TrajetTypeManager($bdd);
          $trajetsTypes = $trajetTypeManager->getListe("LIMIT 0, 1000");
          $tpl->assign(array(
            'trajetsTypes' => $trajetsTypes
            ));
          $tpl->display('specific/submit-selection.html');
        }
      }

      // Si le visiteur n'est pas connecté
      else {
        $_SESSION['popup_type'] = "error";
        $_SESSION['popup_content'] = "Vous devez être connecté pour pouvoir accéder à cette page.";
        header('Location: index.php');
      }
    }

    require_once('includes/footer.php');
