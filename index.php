<?php

    define('Vitoco', true);
    $titre = 'Vitoco | Accueil';
    $cache = false;
/**/
    require_once('includes/init.php');
    require_once('includes/head.php');
    require_once('includes/popup.php');

    $tpl = new Smarty;

    $statistiquesManager = new StatistiquesManager($bdd);
    $statistiques = $statistiquesManager->getStatistiques();

    $dateJour = (new DateTime("now"));
    //$dateBonus1Y = $dateJour->add(new DateInterval("P1Y"));
    $dateBonus1Y = $dateJour;
    $fmtDateJour = $dateJour->format("Y-m-d");
    $fmtDateBonus1Y = $dateBonus1Y->format("Y-m-d");
    $fmtHeure = $dateJour->format("H:i");

    $trajetDisponibleManager = new TrajetDisponibleManager($bdd);

    $membreManager = new MembreManager($bdd);
    $ville = "";
    if ($_SESSION['visiteur']->estConnecte()) {
        $membreManager->loadAdresses([$_SESSION['visiteur']]);
        $ville = $_SESSION['visiteur']->getAdresse()->getVille();
    }


    // Si on a lancé une recherche par critères
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
      $trajetsDisponibles = $trajetDisponibleManager->getListeAccueil ("LIMIT 0,100", $_POST['date'], $_POST['villedep'], $_POST['villearr'], $_POST['prix']);
    }
    // Affichage normal de la page d'index
    else {
        $trajetsDisponibles = $trajetDisponibleManager->getListe("LIMIT 0,100");
      // recupère tous les trajets classés par datetime, à partir de maintenant
    }
    $tpl->assign(array(
        'distanceEconomisee' => $statistiques->getValeur('distanceEconomisee'),
        'ville' => $ville,
        'dateBonus1Y' => $fmtDateBonus1Y,
        'dateJour' => $fmtDateJour,
        'heure' => $fmtHeure,
        'membreManager' => $membreManager,
        'trajetsDisponibles' => $trajetsDisponibles,
        'visiteur' => $_SESSION['visiteur']
    ));

    $tpl->assign(array(
        '_SESSION' => $_SESSION
    ));

    $tpl->display('specific/index.html');

    require_once('includes/footer.php');
