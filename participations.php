<?php

    define('Vitoco', true);
    $titre = 'Liste des trajets';

    $cache = false;

    require_once('includes/init.php');

    require_once('includes/head.php');

    require_once('includes/popup.php');

    $tpl = new Smarty;

    // Le HTML se trouve entièrement dans un fichier séparé.
    $tpl->display('specific/participations.html');

    require_once('includes/footer.php');
