function showUndisplayedBlockElement(id) {
  document.getElementById(id).style.display = "block";
}

function hideDisplayedBlockElement(id) {
  document.getElementById(id).style.display = "none";
}

function addAdditionalStepInSubmitting(i) {
  var eltToCreate = document.createElement("fieldset");
  eltToCreate.class = "form-fieldset";

  eltToCreate.innerHTML = '<legend>Etape intermédiaire</legend>'+
      '<span class="form-span">N° voie :</span>'+
        '<input class="form-input" type="text" name="e'+i+'-num-voie" maxlength="10">'+
      '<span class="form-span">Nom voie :</span>'+
        '<input class="form-input" type="text" name="e'+i+'-nom-voie" maxlength="200">'+
      '<span class="form-span">Complément :</span>'+
        '<input class="form-input" type="text" name="e'+i+'-complement" maxlength="500">'+
      '<span class="form-span">Code Postal :</span>'+
        '<input class="form-input" type="text" name="e'+i+'-cp" maxlength="10">'+
      '<span class="form-span">* Ville :</span>'+
        '<input class="form-input" type="text" name="e'+i+'-ville" maxlength="70" required>';

  document.getElementById("submit-additionals-steps").appendChild(eltToCreate);

  return i + 1;
}
