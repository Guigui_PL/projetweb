<?php

	if ( !defined('Vitoco') ) exit;

	$tplEntete = new Smarty;

	$tplEntete->assign(array(
		'titrePage' => $titre,
		'cheminRacine' => $cheminRacine,
		'roleVisiteur' => $_SESSION['visiteur']->getRole(),
		'visiteurEstConnecte' => $_SESSION['visiteur']->estConnecte()
		));

	$tplEntete->display('generic/head.html');
