<?php

	if ( !defined('Vitoco') ) exit;

	$tplPopup = new Smarty;

	$affichePopup = false;

	if (isset($_SESSION['popup_type']) && isset($_SESSION['popup_content'])) {
		$colorPopup = "#99ccff";
		if ($_SESSION['popup_type'] == "error") $colorPopup = "#ff8566";
    else if ($_SESSION['popup_type'] == "notification") $colorPopup = "#99e699";
	  $tplPopup->assign(array(
	    'corpsPopup' => $_SESSION['popup_content'],
	    'couleurPopup' => $colorPopup
	    ));

		unset($_SESSION['popup_type']);
		unset($_SESSION['popup_content']);

		$affichePopup = true;
	}
