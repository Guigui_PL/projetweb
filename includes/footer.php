<?php

	if ( !defined('Vitoco') ) exit;

	// Écriture du cache
	if (!isset($_SESSION['membre']) && $_SERVER['REQUEST_METHOD'] != 'POST' && $cache) {
		$page = ob_get_contents(); // Copie du contenu du tampon dans une chaîne
		$$nomCache->setContenu($page); // On met le contenu de notre page dans l'objet de cache
		ob_end_clean(); // Effacement du contenu du tampon et arrêt de son fonctionnement
		$cacheManager->writeCache($$nomCache); // On écrit le cache
		echo $page ; // On affiche notre page
	}

	// On calcule le temps de génération de la page à 4 décimales près
	// On prend le temps actuel en secondes avec un niveau de précision en millisecondes, on lui retire le moment à partir duquel la page a commencé à charger contenu dans la superglobale $_SERVER["REQUEST_TIME_FLOAT"]
	$tempsGeneration = round(microtime(true)-$_SERVER["REQUEST_TIME_FLOAT"], 4);

	$tplPied = new Smarty;

	$tplPied->assign(array(
		'tempsGeneration' => $tempsGeneration,
		'requetes' => $bdd->count(),
		'temps' => round($bdd->time(), 4)
		));

	$tplPied->display('generic/footer.html');

	if ($affichePopup) $tplPopup->display('generic/popup.html');
