<?php

if ( !defined('Vitoco') ) exit;

	class CacheArray extends Cache implements ArrayAccess, SeekableIterator, Countable
	{
		private $_position = 0;
		
		public function __construct ($nom, $duree)
		{
			parent::__construct($nom, $duree);
		}
		
		public function setContenu ( $contenu)
		{
			$this->_contenu = $contenu;
		}
		
		public function current()
		{
			return $this->_contenu[$this->_position];
		}
		
		public function key()
		{
			return $this->_position;
		}
		
		public function next()
		{
			$this->_position++;
		}
		
		public function rewind()
		{
			$this->_position = 0;
		}
		
		public function valid()
		{
			return isset($this->_contenu[$this->_position]);
		}
		
		public function seek($position)
		{
			$anciennePosition = $this->_position;
			$this->_position = $position;
			
			if (!$this->valid())
			{
				trigger_error('La position spécifiée n\'est pas valide', E_USER_WARNING);
				$this->_position = $anciennePosition;
			}
		}
		
		public function offsetExists($key)
		{
			return isset($this->_contenu[$key]);
		}
		
		public function offsetGet($key)
		{
			return $this->_contenu[$key];
		}
		
		public function offsetSet($key, $value)
		{
			$this->_contenu[$key] = $value;
		}
		
		public function offsetUnset($key)
		{
			unset($this->_contenu[$key]);
		}
		
		public function count()
		{
			return count($this->_contenu);
		}
		
		public function fetch ()
		{
			if ($this->valid())
			{
				$current = $this->current();
				$this->next();
				return $current;
			}
			else return false;
		}
	}
	
?>