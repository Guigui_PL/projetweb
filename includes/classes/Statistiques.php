<?php

class Statistiques
{
    private $_statistiques;
    
    public function getValeur($nom)
    {
        if (array_key_exists($nom, $this->_statistiques))
            return $this->_statistiques[$nom];
        
        throw new Exception("La statistique ".$nom."n'existe pas !");
    }

    public function setStatistiques(array $stats)
    {
        $this->_statistiques = $stats;
    }

    public function addValeur ($nom, $valeur)
    {
        $this->_statistiques[$nom] = $valeur;
    }
}

