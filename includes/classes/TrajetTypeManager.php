<?php
class TrajetTypeManager
{
    use ToolsForManagers;

    public function __construct ($bdd)
    {
        $this->setBdd($bdd);
    }
    
    public function getResultatsAnalyse ()
    {
        $req = $this->_bdd->query("CALL `analyse_td`();");
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function loadVilles (array &$trajetsTypes)
    {
        $adresseManager = new AdresseManager($this->_bdd);
        foreach ($trajetsTypes as &$trajet)
        {
            $array = [
                $adresseManager->getAdresse(['idAdresse', DB::EGAL, $trajet->getVilleDepart(true)]),
                $adresseManager->getAdresse(['idAdresse', DB::EGAL, $trajet->getVilleArrivee(true)])
            ];
            $trajet->setEtapes($array);
        }
    }

    public function getListe ($limit, $listeParametres = null, $ordre = null)
    {
        if ($listeParametres != null || $ordre != null)
            $champs = $this->listeColonnes(['TrajetType']);
        else $champs = null;

        $requete = "SELECT * FROM TrajetType ";
        $req = $this->executeRequeteListe($requete, $champs, $listeParametres, $ordre, $limit);

        $trajetsTypes = $this->genereListe($req, 'TrajetType');
        if ($trajetsTypes !== false)
        {
            $this->loadVilles($trajetsTypes);
            return $trajetsTypes;
        }
        return false;
    }

    public function getTrajetType (array $listeParametres)
    {
        if ($listeParametres != null || $ordre != null)
            $champs = $this->listeColonnes(['TrajetType']);
        else $champs = null;

        $requete = "SELECT * FROM TrajetType ";
        $req = $this->executeRequeteListe($requete, $champs, $listeParametres, null, ' LIMIT 0, 1');

        $trajetsTypes = $this->genereListe($req, 'TrajetType');
        if ($trajetsTypes !== false)
        {
            $this->loadVilles($trajetsTypes);
            return reset($trajetsTypes);
        }
        return false;
    }

    public function getNombre ()
    {
        $req = $this->_bdd->query("SELECT COUNT(*) AS count FROM TrajetType");

        return $req->fetch(PDO::FETCH_ASSOC)['count'];
    }

    public function delete ($id)
    {
        $req = $this->_bdd->prepare('DELETE FROM TrajetType WHERE idTrajetType = :id');
        return $req->execute(array('id' => $id));
    }

    public function update (TrajetType $trajetType)
    {
        $req = $this->_bdd->prepare('UPDATE TrajetType SET villeArrivee = :villeArrivee, villeDepart = :villeDepart, distance = :distance, tempsIndicatif = :tempsIndicatif, plafondPrixKm = :plafondPrixKm WHERE idTrajetType = :id');
        $req->bindValue(":villeArrivee", $trajetType->getVilleArrivee("id"), PDO::PARAM_STR);
        $req->bindValue(":villeDepart", $trajetType->getVilleDepart("id"), PDO::PARAM_STR);
        $req->bindValue(":distance", $trajetType->getDistance(), PDO::PARAM_STR);
        $req->bindValue(":tempsIndicatif", $trajetType->getTempsIndicatif(), PDO::PARAM_STR);
        $req->bindValue(":plafondPrixKm", $trajetType->getPlafondPrixKm(), PDO::PARAM_STR);
        $req->bindValue(":id", $trajetType->getIdTrajetType(), PDO::PARAM_STR);
        return $req->execute();
    }

    public function insert (TrajetType $trajetType)
    {
        $req = $this->_bdd->prepare('INSERT INTO TrajetType(villeArrivee, villeDepart, distance, tempsIndicatif, plafondPrixKm) VALUES (:villeArrivee, :villeDepart, :distance, :tempsIndicatif, :plafondPrixKm)');
        $req->bindValue(":villeArrivee", $trajetType->getVilleArrivee("id"), PDO::PARAM_STR);
        $req->bindValue(":villeDepart", $trajetType->getVilleDepart("id"), PDO::PARAM_STR);
        $req->bindValue(":distance", $trajetType->getDistance(), PDO::PARAM_STR);
        $req->bindValue(":tempsIndicatif", $trajetType->getTempsIndicatif(), PDO::PARAM_STR);
        $req->bindValue(":plafondPrixKm", $trajetType->getPlafondPrixKm(), PDO::PARAM_STR);
        return $req->execute();
    }
}
