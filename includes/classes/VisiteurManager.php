<?php

class VisiteurManager
{
    use ToolsForManagers;
    
    public function __construct ($bdd)
    {
        $this->setBdd($bdd);
    }
    
    public function getListe ($limit = null, $listeParametres = null, $ordre = null)
    {
        if ($listeParametres != null || $ordre != null)
            $champs = $this->listeColonnes(['Visiteur']);
            else $champs = null;
            
            $requete = "SELECT * FROM Visiteur ";
            if ($limit == null)
                $limit = "LIMIT 0, 10000";
            $req = $this->executeRequeteListe($requete, $champs, $listeParametres, $ordre, $limit);
            
            return $this->genereListe($req, 'Visiteur');
    }
    
    public function getListeAll ()
    {
        $requete = "SELECT * FROM Visiteur ";
        $req = $this->executeRequeteListe($requete, null, null, null, null);
        
        return $this->genereListe($req, 'Visiteur');
    }
    
    public function getNombre ()
    {
        $req = $this->_bdd->query("SELECT valeur AS count FROM Statistiques WHERE nom = 'nbrEnLignes'");
        
        return $req->fetch(PDO::FETCH_ASSOC)['count'];
    }
    
    public function getVisiteur ($idSession)
    {
        $req = $this->_bdd->prepare("SELECT * FROM Visiteur WHERE idSession = :idSession");
        $req->execute(array('idSession' => $idSession));
        $resultat = $req->fetch(PDO::FETCH_ASSOC);
        
        $visiteur = new Visiteur;
        $visiteur->hydrate($resultat);
        return $visiteur;
    }
    
    public function existe ($idSession)
    {
        $req = $this->_bdd->prepare("SELECT COUNT(*) AS count FROM Visiteur WHERE idSession = :idSession");
        $req->execute(array('idSession' => $idSession));
        
        return $req->fetch(PDO::FETCH_ASSOC)['count'] != 0;
    }
    
    public function genereVisiteur ()
    {
        $visiteur = new Visiteur;
        $visiteur->hydrate(array(
            'idSession' => session_id(),
            'IP' => $_SERVER['REMOTE_ADDR'],
            'dateDenierePageVue' => time(),
            'dernierePageVue' => $_SERVER['REQUEST_URI'],
            'navigateur' => $_SERVER['HTTP_USER_AGENT']
        ));
        
        if (empty($_SESSION['visiteur']))
            $_SESSION['visiteur'] = $visiteur;
        return $visiteur;
    }
    
    public function addVisiteur ()
    {
        try 
        {
            $visiteur = $this->genereVisiteur();
            $_SESSION['visiteur'] = $visiteur;
            $req = $this->_bdd->prepare('INSERT INTO Visiteur (idSession, IP, dateDernierePageVue, dernierePageVue, navigateur)
    			VALUES(:id, :IP, :dateDernierePageVue, :dernierePageVue, :navigateur)');
            $req->bindValue(':id', $visiteur->getIdSession(), PDO::PARAM_INT);
            $req->bindValue(':IP', $visiteur->getIP(), PDO::PARAM_STR);
            $req->bindValue(':dateDernierePageVue', $visiteur->getDateDernierePageVue(), PDO::PARAM_INT);
            $req->bindValue(':dernierePageVue', $visiteur->getDernierePageVue(), PDO::PARAM_STR);
            $req->bindValue(':navigateur', $visiteur->getNavigateur(), PDO::PARAM_STR);
            return $req->execute();
        }
        catch (Exception $e)
        {
            return $e->getMessage();
        }
    }
    
    public function delete ($id)
    {
        $req = $this->_bdd->prepare('DELETE FROM Visiteur WHERE idSession = :idSession');
        return $req->execute(array('idSession' => $id));
    }
    
    public function supprimeVisiteursAbsents ($tempsAnonyme, $tempsConnecte)
    {
        $req = $this->_bdd->prepare('DELETE FROM Visiteur WHERE (dateDernierePageVue < :tempsAnonyme AND idMembreConnecte IS NULL) OR (dateDernierePageVue < :tempsConnecte) ');
        return $req->execute(array('tempsAnonyme' => date_timestamp_set(new DateTime(), $tempsAnonyme)->format(MYSQL_DATETIME_FORMAT), 'tempsConnecte' => date_timestamp_set(new DateTime(), $tempsConnecte)->format(MYSQL_DATETIME_FORMAT)));
    }
    
    public function egaux (Visiteur $visiteur1, Visiteur $visiteur2)
    {
        return $visiteur1 == $visiteur2;
    }
    
    public function updateVisiteur (Visiteur $visiteur)
    {
        $req = $this->_bdd->prepare('UPDATE Visiteur SET dateDernierePageVue = :dateDernierePageVue, dernierePageVue = :dernierePageVue, navigateur = :navigateur, IP = :IP, idMembreConnecte = :idMembreConnecte WHERE idSession = :id');
        $req->bindValue(':id', $visiteur->getIdSession(), PDO::PARAM_INT);
        $req->bindValue(':IP', $visiteur->getIP(), PDO::PARAM_STR);
        $req->bindValue(':dateDernierePageVue', $visiteur->getDateDernierePageVue(), PDO::PARAM_INT);
        $req->bindValue(':dernierePageVue', $visiteur->getDernierePageVue(), PDO::PARAM_STR);
        $req->bindValue(':navigateur', $visiteur->getNavigateur(), PDO::PARAM_STR);
        $req->bindValue(':idMembreConnecte', $visiteur->getIdMembreConnecte(), PDO::PARAM_STR);
        return $req->execute();
    }
    
    public function updateVisiteurs ($tempsAnonyme = null, $tempsConnecte = null)
    {
        if ($tempsAnonyme == null) $tempsAnonyme = time()-300;
        if ($tempsConnecte == null) $tempsConnecte = time()-1440;
        $this->supprimeVisiteursAbsents($tempsAnonyme, $tempsConnecte);
        
        if ($this->existe(session_id()))
        {
            $visiteur = $this->getVisiteur(session_id());
            $visiteurAJour = $this->genereVisiteur();
            if (!$this->egaux($visiteur, $visiteurAJour))
            {
                $visiteurAJour->setIdMembreConnecte($visiteur->getIdMembreConnecte());
                $this->updateVisiteur($visiteurAJour);
                if ($_SESSION['visiteur']->estConnecte())
                {
                    $membreManager = new MembreManager($this->_bdd);
                    $_SESSION['visiteur'] = $membreManager->getMembre(['idSession', DB::EGAL, session_id()]);
                }
                else
                    $_SESSION['visiteur'] = $visiteurAJour;
            }
        }
        else
        {
            return $this->addVisiteur();
        }
    }
}

