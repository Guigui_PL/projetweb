<?php

    if ( !defined('Vitoco') ) exit;

	class Adresse
	{
        use Hydrate;

		private $_idAdresse;
		private $_numeroVoie;
		private $_nomVoie;
		private $_CP;
		private $_ville;
		private $_complement;

		/**
         * @return mixed
         */
        public function getComplement()
        {
            return $this->_complement;
        }

        /**
         * @param mixed $_complement
         */
        public function setComplement($_complement)
        {
            $this->_complement = $_complement;
        }

        public function setIdAdresse ($id)
		{
            $this->_idAdresse = $id;
		}

		public function setNumeroVoie($numero)
		{
		    $this->_numeroVoie = intval($numero);
		}

		public function setNomVoie($voie)
		{
			$this->_nomVoie = $voie;
		}

		public function setCP($codePostal)
		{
			$this->_CP = $codePostal;
		}

		public function setVille($ville)
		{
			if (empty($ville))
				throw new exception ("La ville ne peut être vide");

			$this->_ville = $ville;
		}

		public function getIdAdresse() { return $this->_idAdresse; }
		public function getNumeroVoie() { return $this->_numeroVoie; }
		public function getNomVoie() { return $this->_nomVoie; }
		public function getCP() { return $this->_CP; }
		public function getVille() { return $this->_ville; }

		public function __toString ()
		{
		    $chaine = "";
		    if (!empty($this->_numeroVoie))
		        $chaine .= $this->_numeroVoie.' ';
	        if (!empty($this->_nomVoie))
	            $chaine .= $this->_nomVoie.', ';
            if (!empty($this->_complement))
                $chaine .= $this->_complement.', ';
            if (!empty($this->_CP))
                $chaine .= $this->_CP.' ';
            if (!empty($this->_ville))
                $chaine .= $this->_ville.' ';
            return $chaine;
		}
	}
