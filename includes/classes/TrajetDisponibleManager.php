<?php
class TrajetDisponibleManager
{
    use ToolsForManagers, GestionDateHeure;
    
    public function __construct ($bdd)
    {
        $this->setBdd($bdd);
    }
    
    public function loadEtapes (array &$trajetDisponibles)
    {        
        $champs = $this->listeColonnes(['Adresse', 'PassePar']);
        $requete = "SELECT * FROM PassePar
                    JOIN Adresse ON idAdresse = adresse ";
            
        $adresseManager = new AdresseManager($this->_bdd);
        foreach ($trajetDisponibles as &$trajet)
        {
            $listeParametres = ['idTrajet', DB::EGAL, $trajet->getIdTrajetDisponible()];
            $req = $this->executeRequeteListe($requete, $champs, $listeParametres, [['champ' => 'ordre', 'sens' => DB::ORDRE_ASC]], '');
            
            $trajet->setEtapes($this->genereListe($req, 'Adresse'));
        }
    }
    
    public function loadParticipants (array &$trajetDisponibles)
    {
        $req = $this->_bdd->prepare("SELECT * FROM Participe WHERE idParticipation = :idTrajet");
        foreach ($trajetDisponibles as &$trajet)
        {
            $req->execute(array("idTrajet" => $trajet->getIdTrajetDisponible()));
            while ($participant = $req->fetch(PDO::FETCH_ASSOC))
            {
                $trajet->addParticipant($participant["idParticipant"]);
            }
        }
    }
    
    public function getTrajetsDuParticipant ($idMembre, $limit = null)
    {
        $champs = $this->listeColonnes(['TrajetDisponible', 'Participe']);
        
        $requete = "SELECT * FROM TrajetDisponible
                    JOIN Participe ON idTrajetDisponible = idParticipation ";
        $listeParametres = ['idParticipant', DB::EGAL, $idMembre];
        $req = $this->executeRequeteListe($requete, $champs, $listeParametres, [['champ' => 'dateCreation', 'sens' => DB::ORDRE_DESC]], $limit);
        
        $trajets = $this->genereListe($req, 'TrajetDisponible');
        $this->loadEtapes($trajets);
        $this->loadParticipants($trajets);
        return $trajets;
    }
    
    public function getListe ($limit, $listeParametres = null, $ordre = null)
    {
        if ($listeParametres != null || $ordre != null)
            $champs = $this->listeColonnes(['TrajetDisponible']);
        else $champs = null;
        
        $requete = "SELECT * FROM TrajetDisponible ";
        $req = $this->executeRequeteListe($requete, $champs, $listeParametres, $ordre, $limit);
        
        $trajets = $this->genereListe($req, 'TrajetDisponible');
        $this->loadEtapes($trajets);
        $this->loadParticipants($trajets);
        return $trajets;
    }
    
    public function getListeAccueil ($limit, $date, $villedep, $villearr, $prix)
    {
        $requete = "SELECT d.ville As ville_depart, a.ville AS ville_arrivee, TrajetDisponible.* FROM TrajetDisponible, PassePar ppd, PassePar ppa, Adresse d, Adresse a
          WHERE
            idTrajetDisponible = ppd.idTrajet AND idTrajetDisponible = ppa.idTrajet AND
            d.idAdresse = ppd.adresse AND a.idAdresse = ppa.adresse AND
            ppd.ordre = (SELECT ordre FROM PassePar pt WHERE pt.idTrajet = ppd.idTrajet ORDER BY ordre ASC LIMIT 0, 1) AND
            ppa.ordre = (SELECT ordre FROM PassePar pt WHERE pt.idTrajet = ppa.idTrajet ORDER BY ordre DESC LIMIT 0, 1)
          ";
            
        $execute = [];
        if (!empty($date))
        {
            $date = $this->creerDateTime($date);
            $requete .= " AND DATE_FORMAT(dateTrajet,  '%Y-%m-%d') = :dateTrajet";
            $execute["dateTrajet"] = $date->format(MYSQL_DATE_FORMAT);
        }
        if (!empty($villedep))
        {
            $requete .= " AND d.ville = :ville_depart";
            $execute["ville_depart"] = $villedep;
        }
        if (!empty($villearr))
        {
            $requete .= " AND a.ville = :ville_arrivee";
            $execute["ville_arrivee"] = $villearr;
        }
        if (!empty($prix))
        {
            $requete .= " AND prixKm < :prixKm";
            $execute["prixKm"] = $prix;
        }
            
        $req = $this->_bdd->prepare($requete);
        $req->execute($execute);
        
        $retour = [];
        while ($ligne = $req->fetch())
        {
            $trajet = new TrajetDisponible();
            $trajet->hydrate($ligne);
            $retour[] = $trajet;
        }
        $this->loadEtapes($retour);
        $this->loadParticipants($retour);
        
        return $retour;
    }
    
    public function getTrajetDisponible (array $listeParametres)
    {
        if ($listeParametres != null || $ordre != null)
            $champs = $this->listeColonnes(['TrajetDisponible']);
            else $champs = null;
            
            $requete = "SELECT * FROM TrajetDisponible ";
            $req = $this->executeRequeteListe($requete, $champs, $listeParametres, null, ' LIMIT 0, 1');
            
            $trajets = $this->genereListe($req, 'TrajetDisponible');
            $this->loadEtapes($trajets);
            $this->loadParticipants($trajets);
            return reset($trajets);
    }
    
    public function getNombre ()
    {
        $req = $this->_bdd->query("SELECT COUNT(*) AS count FROM TrajetDisponible");
        
        return $req->fetch(PDO::FETCH_ASSOC)['count'];
    }
    
    public function delete ($id)
    {
        $req = $this->_bdd->prepare('DELETE FROM TrajetDisponible WHERE idTrajetDisponible = :id');
        return $req->execute(array('id' => $id));
    }
    
    public function update (TrajetDisponible $trajetDisponible)
    {
        $req = $this->_bdd->prepare('UPDATE TrajetDisponible SET conducteur = :conducteur, distance = :distance, dateTrajet = :dateTrajet, dureeEstimee = :dureeEstimee, prixKm = :prixKm, description = :description, cigaretteAutorisee = :cigaretteAutorisee, bagagesAutorises = :bagagesAutorises, animalAutorise = :animalAutorise, dateCreation = :dateCreation, vehicule = :vehicule, implemente = :implemente, effectue = :effectue WHERE idTrajetDisponible = :id');
        $req->bindValue(":conducteur", $trajetDisponible->getConducteur(), PDO::PARAM_STR);
        $req->bindValue(":distance", $trajetDisponible->getDistance(), PDO::PARAM_STR);
        $req->bindValue(":dateTrajet", $trajetDisponible->getDateTrajet(MYSQL_DATETIME_FORMAT), PDO::PARAM_STR);
        $req->bindValue(":dureeEstimee", $trajetDisponible->getDureeEstimee(), PDO::PARAM_STR);
        $req->bindValue(":prixKm", $trajetDisponible->getPrixKm(), PDO::PARAM_STR);
        $req->bindValue(":description", $trajetDisponible->getDescription(), PDO::PARAM_STR);
        $req->bindValue(":cigaretteAutorisee", $trajetDisponible->getCigaretteAutorisee(), PDO::PARAM_STR);
        $req->bindValue(":bagagesAutorises", $trajetDisponible->getBagagesAutorises(), PDO::PARAM_STR);
        $req->bindValue(":animalAutorise", $trajetDisponible->getAnimalAutorise(), PDO::PARAM_STR);
        $req->bindValue(":dateCreation", $trajetDisponible->getDateCreation(MYSQL_DATETIME_FORMAT), PDO::PARAM_STR);
        $req->bindValue(":vehicule", $trajetDisponible->getVehicule(), PDO::PARAM_STR);
        $req->bindValue(":implemente", $trajetDisponible->getImplemente(), PDO::PARAM_STR);
        $req->bindValue(":effectue", $trajetDisponible->getEffectue(), PDO::PARAM_STR);
        $req->bindValue(":id", $trajetDisponible->getIdTrajetDisponible(), PDO::PARAM_STR);
        return $req->execute();
    }
    
    public function insert (TrajetDisponible $trajetDisponible)
    {
        $req = $this->_bdd->prepare('INSERT INTO TrajetDisponible(conducteur, distance, dateTrajet, dureeEstimee, prixKm, description, cigaretteAutorisee, bagagesAutorises, animalAutorise, dateCreation, vehicule, implemente, effectue) VALUES (:conducteur, :distance, :dateTrajet, :dureeEstimee, :prixKm, :description, :cigaretteAutorisee, :bagagesAutorises, :animalAutorise, NOW(), :vehicule, :implemente, :effectue)');
        $req->bindValue(":conducteur", $trajetDisponible->getConducteur(), PDO::PARAM_STR);
        $req->bindValue(":distance", $trajetDisponible->getDistance(), PDO::PARAM_STR);
        $req->bindValue(":dateTrajet", $trajetDisponible->getDateTrajet(MYSQL_DATETIME_FORMAT), PDO::PARAM_STR);
        $req->bindValue(":dureeEstimee", $trajetDisponible->getDureeEstimee(), PDO::PARAM_STR);
        $req->bindValue(":prixKm", $trajetDisponible->getPrixKm(), PDO::PARAM_STR);
        $req->bindValue(":description", $trajetDisponible->getDescription(), PDO::PARAM_STR);
        $req->bindValue(":cigaretteAutorisee", $trajetDisponible->getCigaretteAutorisee(), PDO::PARAM_STR);
        $req->bindValue(":bagagesAutorises", $trajetDisponible->getBagagesAutorises(), PDO::PARAM_STR);
        $req->bindValue(":animalAutorise", $trajetDisponible->getAnimalAutorise(), PDO::PARAM_STR);
        $req->bindValue(":vehicule", $trajetDisponible->getVehicule(), PDO::PARAM_STR);
        $req->bindValue(":implemente", $trajetDisponible->getImplemente(), PDO::PARAM_STR);
        $req->bindValue(":effectue", $trajetDisponible->getEffectue(), PDO::PARAM_STR);
        if ($req->execute() !== FALSE)
        {
            $idTrajet = $this->_bdd->lastInsertId();
            $this->insertEtapes($trajetDisponible->getEtapes(), $idTrajet);
            if (!empty($trajetDisponible->getParticipants()))
                $this->insertParticipants($trajetDisponible->getParticipants(), $idTrajet);
            return $idTrajet;
        }
        return false;
    }
    
    public function insertEtapes (array $etapes, $idTrajet)
    {
        $req = $this->_bdd->prepare("INSERT INTO PassePar (idTrajet, adresse, ordre) VALUES (:idTrajet, :adresse, :ordre)");
        
        $ordre = 0;
        foreach ($etapes as $etape)
        {
            $req->execute(array("idTrajet" => $idTrajet,
                                "adresse" => $etape->getIdAdresse(),
                                "ordre" => $ordre
            ));
            $ordre++;
        }
    }
    
    public function insertParticipants (array $participants, $idTrajet)
    {
        $req = $this->_bdd->prepare("INSERT INTO Participe (idParticipant, idParticipation) VALUES (:idParticipant, :idParticipation)");
        
        foreach ($participants as $participant)
        {
            $req->execute(array("idParticipant" => $participant,
                                "idParticipation" => $idTrajet
            ));
        }
    }
}

