<?php

if ( !defined('Vitoco') ) exit;

	abstract class Trajet
	{
		protected $_villeDepart;
		protected $_villeArrivee;
		protected $_etapes            = [];
		protected $_distance;
		protected $_tempsIndicatif;

		public function __construct ()
		{
		    $this->_etapes = [];
			$this->_villeDepart = new Adresse;
			$this->_villeArrivee = new Adresse;
		}

		public function setVilleDepart ($depart)
		{
		    if (!($depart instanceof Adresse))
		    {
		        $id = $depart;
		        $depart = new Adresse();
		        $depart->setIdAdresse($id);
		        $this->_villeDepart = $depart;
		    }
		    
		    else if (!$this->existeEtape($depart))
		    {
		        array_unshift($this->_etapes, $depart);
		        $this->_villeDepart = reset($this->_etapes);
		    }
		}

		public function setVilleArrivee ($arrivee)
		{
		    if (!($arrivee instanceof Adresse))
		    {
		        $id = $arrivee;
		        $arrivee = new Adresse();
		        $arrivee->setIdAdresse($id);
		        $this->_villeArrivee = $arrivee;
		    }
		    
		    else if (!$this->existeEtape($arrivee))
		    {
		        $this->_etapes[] = $arrivee;
		        $this->_villeArrivee = end($this->_etapes);
		    }
		}

		public function setEtapes (array $etapes)
		{
		    $this->_etapes = $etapes;
		    $this->setVilleDepart(reset($this->_etapes));
		    $this->setVilleArrivee(end($this->_etapes));
		}

		public function addEtape (Adresse $etape)
		{
			$this->_etapes[] = $etape;
			$this->setVilleDepart(reset($this->_etapes));
			$this->setVilleArrivee(end($this->_etapes));
		}

		public function removeEtape (Adresse $etape)
		{
			foreach ($this->_etapes as $key => $value)
			{
				if ($value == $etape)
				{
					unset($this->_etapes[$key]);
					return;
				}
			}
			$this->setVilleDepart(reset($this->_etapes));
			$this->setVilleArrivee(end($this->_etapes));
		}
		
		public function existeEtape (Adresse $etape)
		{
		    foreach ($this->_etapes as $key => $value)
		    {
		        if ($value->getIdAdresse() == $etape->getIdAdresse())
		        {
		            return true;
		        }
		    }
		    return false;
		}

		public function setDistance ($distance)
		{
			$this->_distance = intval($distance);
		}

		public function setTempsIndicatif ($temps)
		{
			$this->_tempsIndicatif = intval($temps);
		}

		public function getVilleDepart($wantID = null) 
		{ 
		    if (!empty($this->_etapes))
		        $this->_villeDepart = reset($this->_etapes);
		    if ($wantID && ($this->_villeDepart instanceof Adresse))
		        return $this->_villeDepart->getIdAdresse();
	        return $this->_villeDepart; 
		}
		public function getVilleArrivee($wantID = null)
		{
		    if (!empty($this->_etapes))
		        $this->_villeArrivee = end($this->_etapes);
		    if ($wantID && ($this->_villeArrivee instanceof Adresse))
		        return $this->_villeArrivee->getIdAdresse();
	        return $this->_villeArrivee;
		}
		public function getEtapes() { return $this->_etapes; }
		public function getDistance() { return $this->_distance; }
		public function getTempsIndicatif() { return $this->_tempsIndicatif; }
	}
