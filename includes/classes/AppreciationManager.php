<?php

class AppreciationManager
{
    use ToolsForManagers;
    
    public function __construct ($bdd)
    {
        $this->setBdd($bdd);
    }
    
    public function getListe ($limit, $listeParametres = null, $ordre = null)
    {
        if ($listeParametres != null || $ordre != null)
            $champs = $this->listeColonnes(['Appreciation', 'TrajetDisponible']);
            else $champs = null;
            
            $requete = "SELECT * FROM Appreciation 
                        JOIN TrajetDisponible ON idTrajetDisponible = idTrajet ";
            $req = $this->executeRequeteListe($requete, $champs, $listeParametres, $ordre, $limit);
            
            return $this->genereListe($req, 'Appreciation');
    }
    
    public function getAppreciation (array $listeParametres)
    {
        if ($listeParametres != null || $ordre != null)
            $champs = $this->listeColonnes(['Appreciation']);
            else $champs = null;
            
            $requete = "SELECT * FROM Appreciation ";
            $req = $this->executeRequeteListe($requete, $champs, $listeParametres, null, ' LIMIT 0, 1');
            
            return $this->genereListe($req, 'Appreciation')[0];
    }
    
    public function getNombre ()
    {
        $req = $this->_bdd->query("SELECT COUNT(*) AS count FROM Appreciation");
        
        return $req->fetch(PDO::FETCH_ASSOC)['count'];
    }
    
    public function delete ($id)
    {
        $req = $this->_bdd->prepare('DELETE FROM Appreciation WHERE idAppreciation = :id');
        return $req->execute(array('id' => $id));
    }
    
    public function update (Appreciation $appreciation)
    {
        $req = $this->_bdd->prepare('UPDATE Appreciation SET corps = :corps, note = :note WHERE idAppreciation = :id');
        $req->bindValue(":corps", $appreciation->getCorps(), PDO::PARAM_STR);
        $req->bindValue(":note", $appreciation->getNote(), PDO::PARAM_STR);
        $req->bindValue(":id", $appreciation->getIdAppreciation(), PDO::PARAM_STR);
        return $req->execute();
    }
    
    public function insert (Appreciation $appreciation)
    {
        $req = $this->_bdd->prepare('INSERT INTO Appreciation(corps, note, idAuteur, idTrajet) VALUES (:corps, :note, :idAuteur, :idTrajet)');
        $req->bindValue(":corps", $appreciation->getCorps(), PDO::PARAM_STR);
        $req->bindValue(":note", $appreciation->getNote(), PDO::PARAM_STR);
        $req->bindValue(":idAuteur", $appreciation->getIdAuteur(), PDO::PARAM_STR);
        $req->bindValue(":idTrajet", $appreciation->getIdTrajet(), PDO::PARAM_STR);
        
        return $req->execute();
    }
}

