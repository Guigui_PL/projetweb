<?php

class MessageManager
{
    use ToolsForManagers;
    
    public function __construct ($bdd)
    {
        $this->setBdd($bdd);
    }
    
    public function getListe ($limit, $listeParametres = null, $ordre = null)
    {
        if ($listeParametres != null || $ordre != null)
            $champs = $this->listeColonnes(['Message', 'TrajetDisponible']);
            else $champs = null;
            
            $requete = "SELECT * FROM Message
                        JOIN TrajetDisponible ON idTrajetDisponible = idTrajet ";
            $req = $this->executeRequeteListe($requete, $champs, $listeParametres, $ordre, $limit);
            
            return $this->genereListe($req, 'Message');
    }
    
    public function getMessage (array $listeParametres)
    {
        if ($listeParametres != null || $ordre != null)
            $champs = $this->listeColonnes(['Message']);
            else $champs = null;
            
            $requete = "SELECT * FROM Message ";
            $req = $this->executeRequeteListe($requete, $champs, $listeParametres, null, ' LIMIT 0, 1');
            
            return $this->genereListe($req, 'Message')[0];
    }
    
    public function getNombre ()
    {
        $req = $this->_bdd->query("SELECT COUNT(*) AS count FROM Message");
        
        return $req->fetch(PDO::FETCH_ASSOC)['count'];
    }
    
    public function delete ($id)
    {
        $req = $this->_bdd->prepare('DELETE FROM Message WHERE idMessage = :id');
        return $req->execute(array('id' => $id));
    }
    
    public function update (Message $message)
    {
        $req = $this->_bdd->prepare('UPDATE Message SET corps = :corps WHERE idMessage = :id');
        $req->bindValue(":corps", $message->getCorps(), PDO::PARAM_STR);
        $req->bindValue(":id", $message->getIdMessage(), PDO::PARAM_STR);
        return $req->execute();
    }
    
    public function insert (Message $message)
    {
        $req = $this->_bdd->prepare('INSERT INTO Message(corps, idAuteur, idTrajet) VALUES (:corps, :idAuteur, :idTrajet)');
        $req->bindValue(":corps", $message->getCorps(), PDO::PARAM_STR);
        $req->bindValue(":idAuteur", $message->getIdAuteur(), PDO::PARAM_STR);
        $req->bindValue(":idTrajet", $message->getIdTrajet(), PDO::PARAM_STR);
        
        return $req->execute();
    }
}

