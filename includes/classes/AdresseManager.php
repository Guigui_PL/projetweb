<?php

class AdresseManager
{
    use ToolsForManagers;
    
    public function __construct ($bdd)
    {
        $this->setBdd($bdd);
    }
    
    public function getListe ($limit, $listeParametres = null, $ordre = null)
    {
        if ($listeParametres != null || $ordre != null)
            $champs = $this->listeColonnes(['Adresse']);
            else $champs = null;
            
            $requete = "SELECT * FROM Adresse ";
            $req = $this->executeRequeteListe($requete, $champs, $listeParametres, $ordre, $limit);
            
            return $this->genereListe($req, 'Adresse');
    }
    
    public function getAdresse (array $listeParametres)
    {
        if ($listeParametres != null || $ordre != null)
            $champs = $this->listeColonnes(['Adresse']);
            else $champs = null;
            
            $requete = "SELECT * FROM Adresse ";
            $req = $this->executeRequeteListe($requete, $champs, $listeParametres, null, ' LIMIT 0, 1');
            
            return $this->genereListe($req, 'Adresse')[0];
    }
    
    public function getNombre ()
    {
        $req = $this->_bdd->query("SELECT COUNT(*) AS count FROM Adresse");
        
        return $req->fetch(PDO::FETCH_ASSOC)['count'];
    }
    
    public function delete ($id)
    {
        $req = $this->_bdd->prepare('DELETE FROM Adresse WHERE idAdresse = :id');
        return $req->execute(array('id' => $id));
    }
    
    public function update (Adresse $adresse)
    {
        $req = $this->_bdd->prepare('UPDATE Adresse SET numeroVoie = :numeroVoie, nomVoie = :nomVoie, complement = :complement, CP = :CP, ville = :ville WHERE idAdresse = :id');
        $req->bindValue(":numeroVoie", $adresse->getNumeroVoie(), PDO::PARAM_STR);
        $req->bindValue(":nomVoie", $adresse->getNomVoie(), PDO::PARAM_STR);
        $req->bindValue(":complement", $adresse->getComplement(), PDO::PARAM_STR);
        $req->bindValue(":CP", $adresse->getCP(), PDO::PARAM_STR);
        $req->bindValue(":ville", $adresse->getVille(), PDO::PARAM_STR);
        $req->bindValue(":id", $adresse->getIdAdresse(), PDO::PARAM_STR);
        return $req->execute();
    }
    
    public function insert (Adresse $adresse)
    {
        $req = $this->_bdd->prepare('INSERT INTO Adresse(numeroVoie, nomVoie, complement, CP, ville) VALUES (:numeroVoie, :nomVoie, :complement, :CP, :ville)');
        $req->bindValue(":numeroVoie", $adresse->getNumeroVoie(), PDO::PARAM_STR);
        $req->bindValue(":nomVoie", $adresse->getNomVoie(), PDO::PARAM_STR);
        $req->bindValue(":complement", $adresse->getComplement(), PDO::PARAM_STR);
        $req->bindValue(":CP", $adresse->getCP(), PDO::PARAM_STR);
        $req->bindValue(":ville", $adresse->getVille(), PDO::PARAM_STR);
        if ($req->execute() !== FALSE)
        {
            $id = $this->_bdd->lastInsertId();
            $adresse->setIdAdresse($id);
            return $id;
        }
        return false;
    }
}

