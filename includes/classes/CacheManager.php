<?php

if ( !defined('Vitoco') ) exit;
	
	class CacheManager
	{
		protected $_repertoire;
		protected $_chemin;
		protected $_expire;
		
		public function __construct ()
		{
			$this->setRepertoire($GLOBALS['cheminRacine'].'cache/');
		}
		
		public function readCache (Cache $objetCache)
		{
			$this->setExpire(time()-$objetCache->getDuree());
			$this->setChemin($objetCache->getNom());
			
			if(file_exists($this->_chemin) && (filemtime($this->_chemin) > $this->_expire || $objetCache->getDuree() == 0) ) // 0 = pas de raffraichissement du cache
			{
				$objetCache->setContenu(unserialize(file_get_contents($this->_chemin)));
				return true;
			}
			else return false;
		}
		
		public function writeCache (Cache $objetCache)
		{
			$this->setChemin($objetCache->getNom());
			$this->setExpire(time()+$objetCache->getDuree());
			
			file_put_contents($this->_chemin, serialize($objetCache->getContenu()));
		}
		
		public function videCache (Cache $objetCache)
		{
			$this->setChemin($objetCache->getNom());
			if (file_exists($this->_chemin)) ($this->_chemin);
		}
		
		public function setRepertoire ($repertoire)
		{
			if (!is_string($repertoire)) 
			{
				trigger_error('Le chemin du répertoire doit être une chaîne de caractères.', E_USER_WARNING);
				return;
			}
			$this->_repertoire = $repertoire;
		}
		
		public function setChemin ($fichier)
		{
			if (!is_string($fichier) || !isset($this->_repertoire)) 
			{
				trigger_error('Le fichier doit être une chaine de caractères et le répertoire doit être défini.', E_USER_WARNING);
				return;
			}
			$this->_chemin = $this->_repertoire.$fichier;
		}
		
		public function setExpire ($expire)
		{
			if (!is_int($expire)) 
			{
				trigger_error('$expire doit être un entier indiquant le temps en secondes.', E_USER_WARNING);
				return;
			}
			$this->_expire = $expire;
		}
		
		public function viderAllCache ()
		{
			$this->setRepertoire($GLOBALS['cheminRacine'].'cache/');
			$handle=opendir($this->_repertoire);
			while (false !== ($fichier = readdir($handle))) 
				if (($fichier != ".") && ($fichier != "..")) 
					unlink($this->_repertoire.$fichier);
		}
		
		public function videCategorieCache ($nom)
		{
			if (!empty($nom))
			{
				$this->setRepertoire($GLOBALS['cheminRacine'].'cache/');
				$handle=opendir($this->_repertoire);
				while (false !== ($fichier = readdir($handle))) 
					if (($fichier != ".") && ($fichier != "..") && preg_match("/".$nom."/i", $fichier)) 
						unlink($this->_repertoire.$fichier);
			}
		}
	}
	