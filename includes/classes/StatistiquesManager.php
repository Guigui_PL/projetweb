<?php

class StatistiquesManager
{
    use ToolsForManagers;
    
    public function __construct ($bdd)
    {
        $this->setBdd($bdd);
    }
    
    public function getStatistiques ()
    {
        $req = $this->_bdd->query("SELECT * FROM Statistiques");
        
        $statistiques = new Statistiques();
        while ($stat = $req->fetch(PDO::FETCH_ASSOC))
        {
            $statistiques->addValeur($stat["nom"], $stat["valeur"]);
        }
        
        return $statistiques;
    }
}

