<?php

if ( !defined('Vitoco') ) exit;

	class TrajetType extends Trajet
	{
	    use Hydrate;
	    
		private $_idTrajetType;
		private $_plafondPrixKm;
        /**
         * @return mixed
         */
        public function getIdTrajetType()
        {
            return $this->_idTrajetType;
        }
    
        /**
         * @return mixed
         */
        public function getPlafondPrixKm()
        {
            return $this->_plafondPrixKm;
        }
    
        /**
         * @param mixed $_idTrajetType
         */
        public function setIdTrajetType($_idTrajetType)
        {
            $this->_idTrajetType = $_idTrajetType;
        }
    
        /**
         * @param mixed $_plafondPrixKm
         */
        public function setPlafondPrixKm($_plafondPrixKm)
        {
            $this->_plafondPrixKm = $_plafondPrixKm;
        }
    
		
		
	} 
 
