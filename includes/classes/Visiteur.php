<?php

if ( !defined('Vitoco') ) exit;

	class Visiteur
	{
		use GestionDateHeure, Hydrate;
		
		public static $Droits = array(
								"voirInfosUtilisateurs" => 0,
								"creerTrajetType" => 1,
								"Bannir" => 2,
								"voirStatistiques" => 3,
								"proposerTrajet" => 4,
								"listerTrajets" => 5,
								"sInscrireTrajet" => 6);
		
		protected $_idSession;
		protected $_role;
		protected $_IP;
		protected $_dernierePageVue;
		protected $_dateDernierePageVue;
		protected $_navigateur;
		protected $_idMembreConnecte;
		protected $_droitsDuVisiteur;
		
		const ROLE_INVITE = 0;
		const ROLE_MEMBRE = 'Membre';
		const ROLE_ADMIN = 'Administrateur';
		
		/**
         * @return mixed
         */
        public function getIdSession()
        {
            return $this->_idSession;
        }
    
        /**
         * @param mixed $_idSession
         */
        public function setIdSession($_idSession)
        {
            $this->_idSession = $_idSession;
        }
    
        public function __construct()
		{
		    $this->setRole(self::ROLE_INVITE);
		    $this->_droitsDuVisiteur = array(
		        self::$Droits["voirInfosUtilisateurs"] => false,
		        self::$Droits["creerTrajetType"] => false,
		        self::$Droits["Bannir"] => false,
		        self::$Droits["voirStatistiques"] => false,
		        self::$Droits["proposerTrajet"] => false,
		        self::$Droits["listerTrajets"] => false,
		        self::$Droits["sInscrireTrajet"] => false);
		}
		
		/**
         * @return mixed
         */
        public function getIdMembreConnecte()
        {
            return $this->_idMembreConnecte;
        }
    
        /**
         * @param mixed $_idMembreConnecte
         */
        public function setIdMembreConnecte($_idMembreConnecte)
        {
            $this->_idMembreConnecte = $_idMembreConnecte;
        }
		
		protected function setRole ($role)
		{
			if (in_array($role, [self::ROLE_INVITE, self::ROLE_MEMBRE, self::ROLE_ADMIN]))
				$this->_role = $role;
			else 
				throw new Exception("Ce rôle n'existe pas !");
				
            switch ($this->_role)
            {
                case self::ROLE_INVITE:
                    $this->_droits[self::$Droits["listerTrajets"]] = true;
                break;
                
                case self::ROLE_MEMBRE:
                    $this->_droits[self::$Droits["voirInfosUtilisateurs"]] = true;
                    $this->_droits[self::$Droits["proposerTrajet"]] = true;
                    $this->_droits[self::$Droits["listerTrajets"]] = true;
                    $this->_droits[self::$Droits["sInscrireTrajet"]] = true;
                break;
                
                case self::ROLE_ADMIN:
                    foreach ($this->_droits as &$droit)
                        $droit = true;
                break;
            }
		}
		
		public function setIP ($ip)
		{
			if (!is_string($ip) || inet_pton($ip) === FALSE) 
				return false; 
			$this->_IP = $ip;
		}
		
		public function setDernierePageVue ($dPV)
		{
			$this->_dernierePageVue = $dPV;
		}
		
		public function setDateDernierePageVue ($dDPV)
		{
			$this->_dateDernierePageVue = $dDPV;
		}
		
		public function getDroit ($droit)
		{
			if (in_array($droit, $this->_droits))
				return $this->_droits[$droit];
			else 
				throw new Exception("Ce type de droit n'existe pas !");
		}
		
		public function getNomRole()
		{
            switch ($this->_role)
            {
                case self::ROLE_INVITE:
                
                    return "Invité";
            
                case self::ROLE_MEMBRE:
                
                    return "Membre";
                
                case self::ROLE_ADMIN:
                
                    return "Administrateur";
            }
            
		}
		
		public function getNomAffiche(Visiteur $utilisateurDemandeur)
		{
            return "Anonyme";
		}
		
		public function setNavigateur ($agent)
		{
			$this->_navigateur = $agent;
		}
		
		public function estConnecte()
		{
		    return false;
		}
		
		public function getRole() { return $this->_role; }
		public function getIP() { return $this->_IP; }
		public function getDernierePageVue() { return $this->_dernierePageVue; }
		public function getNavigateur() { return $this->_navigateur; }
		public function getDateDernierePageVue() { return $this->_dateDernierePageVue; }
		
		static function __set_state(array $array)
		{
		    $tmp = new Visiteur();
		    $tmp->_idSession = $array['_idSession'];
		    $tmp->_role = $array['_role'];
		    $tmp->_IP = $array['_IP'];
		    $tmp->_dernierePageVue = $array['_dernierePageVue'];
		    $tmp->_dateDernierePageVue = $array['_dateDernierePageVue'];
		    $tmp->_navigateur = $array['_navigateur'];
		    $tmp->_idMembreConnecte = $array['_idMembreConnecte'];
		    $tmp->_droitsDuVisiteur = $array['_droitsDuVisiteur'];
		    return $tmp;
		}
	} 
 
