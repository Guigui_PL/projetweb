<?php

if ( !defined('Vitoco') ) exit;

	class Message
	{
	    use Hydrate;
        protected $_idMessage;
        protected $_corps;
        protected $_idAuteur;
        protected $_idTrajet;
        /**
         * @return mixed
         */
        public function getIdMessage()
        {
            return $this->_idMessage;
        }
    
        /**
         * @return mixed
         */
        public function getCorps()
        {
            return $this->_corps;
        }
    
        /**
         * @return mixed
         */
        public function getIdAuteur()
        {
            return $this->_idAuteur;
        }
    
        /**
         * @return mixed
         */
        public function getIdTrajet()
        {
            return $this->_idTrajet;
        }
    
        /**
         * @param mixed $_idMessage
         */
        public function setIdMessage($_idMessage)
        {
            $this->_idMessage = $_idMessage;
        }
    
        /**
         * @param mixed $_contenu
         */
        public function setCorps($_contenu)
        {
            $this->_corps = $_contenu;
        }
    
        /**
         * @param mixed $_idAuteur
         */
        public function setIdAuteur($_idAuteur)
        {
            $this->_idAuteur = $_idAuteur;
        }
    
        /**
         * @param mixed $_idTrajet
         */
        public function setIdTrajet($_idTrajet)
        {
            $this->_idTrajet = $_idTrajet;
        }
    
	} 
 
