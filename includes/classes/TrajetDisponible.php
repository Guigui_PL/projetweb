<?php

if ( !defined('Vitoco') ) exit;

	class TrajetDisponible extends Trajet
	{
	    use Hydrate, GestionDateHeure;
		private $_idTrajetDisponible;
		private $_conducteur;
		private $_dateTrajet;
		private $_dureeEstimee;
		private $_prixKm;
		private $_description;
		private $_cigaretteAutorisee  = 0;
		private $_bagagesAutorises    = 'SANS';
		private $_animalAutorise      = 0;
		private $_dateCreation;
		private $_vehicule;
		private $_implemente;
		private $_effectue;
		private $_participants;
        /**
         * @return mixed
         */
        public function getIdTrajetDisponible()
        {
            return $this->_idTrajetDisponible;
        }

        /**
         * @return mixed
         */
        public function getConducteur()
        {
            return $this->_conducteur;
        }

        /**
         * @return mixed
         */
        public function getDateTrajet($conversion = null) 
        { return $this->getDateTime($this->_dateTrajet, $conversion); }

        /**
         * @return mixed
         */
        public function getDureeEstimee()
        {
            return $this->_dureeEstimee;
        }

        /**
         * @return mixed
         */
        public function getPrixKm()
        {
            return $this->_prixKm;
        }

				public function getPrix()
				{
						return $this->_prixKm * $this->_distance;
				}

        /**
         * @return mixed
         */
        public function getDescription()
        {
            return $this->_description;
        }

        /**
         * @return mixed
         */
        public function getCigaretteAutorisee()
        {
            return $this->_cigaretteAutorisee;
        }

        /**
         * @return mixed
         */
        public function getBagagesAutorises()
        {
            return $this->_bagagesAutorises;
        }

        /**
         * @return mixed
         */
        public function getAnimalAutorise()
        {
            return $this->_animalAutorise;
        }

        /**
         * @return mixed
         */
        public function getDateCreation ($conversion = null)
        { return $this->getDateTime($this->_dateCreation, $conversion); }

        /**
         * @return mixed
         */
        public function getVehicule()
        {
            return $this->_vehicule;
        }

        /**
         * @return mixed
         */
        public function getImplemente()
        {
            return $this->_implemente;
        }

        /**
         * @return mixed
         */
        public function getEffectue()
        {
            return $this->_effectue;
        }

        /**
         * @return mixed
         */
        public function getParticipants()
        {
            return $this->_participants;
        }

        /**
         * @param mixed $_idTrajetDisponible
         */
        public function setIdTrajetDisponible($_idTrajetDisponible)
        {
            $this->_idTrajetDisponible = $_idTrajetDisponible;
        }

        /**
         * @param mixed $_conducteur
         */
        public function setConducteur($_conducteur)
        {
            $this->_conducteur = $_conducteur;
        }

        /**
         * @param mixed $_dateTrajet
         */
        public function setDateTrajet($_dateTrajet)
        {
            $dateTime = $this->creerDateTime($_dateTrajet);
            if ($dateTime !== FALSE)
            {
                $this->_dateTrajet = $dateTime;
                return true;
            }
            else return false;
        }

        /**
         * @param mixed $_dureeEstimee
         */
        public function setDureeEstimee($_dureeEstimee)
        {
            $this->_dureeEstimee = $_dureeEstimee;
        }

        /**
         * @param mixed $_prixKm
         */
        public function setPrixKm($_prixKm)
        {
            $this->_prixKm = $_prixKm;
        }

        /**
         * @param mixed $_description
         */
        public function setDescription($_description)
        {
            $this->_description = $_description;
        }

        /**
         * @param mixed $_cigaretteAutorisee
         */
        public function setCigaretteAutorisee($_cigaretteAutorisee)
        {
            $this->_cigaretteAutorisee = (int) $_cigaretteAutorisee;
        }

        /**
         * @param mixed $_bagagesAutorises
         */
        public function setBagagesAutorises($_bagagesAutorises)
        {
            $this->_bagagesAutorises = $_bagagesAutorises;
        }

        /**
         * @param mixed $_animalAutorise
         */
        public function setAnimalAutorise($_animalAutorise)
        {
            $this->_animalAutorise = (int) $_animalAutorise;
        }

        /**
         * @param mixed $_dateCreation
         */
        public function setDateCreation($_dateCreation)
        {
            $dateTime = $this->creerDateTime($_dateCreation);
            if ($dateTime !== FALSE)
            {
                $this->_dateCreation = $dateTime;
                return true;
            }
            else return false;
        }

        /**
         * @param mixed $_vehicule
         */
        public function setVehicule($_vehicule)
        {
            $this->_vehicule = $_vehicule;
        }

        /**
         * @param mixed $_implemente
         */
        public function setImplemente($_implemente)
        {
            $this->_implemente = $_implemente;
        }

        /**
         * @param mixed $_effectue
         */
        public function setEffectue($_effectue)
        {
            $this->_effectue = $_effectue;
        }

        /**
         * @param mixed $_participants
         */
        public function setParticipants($_participants)
        {
            $this->_participants = $_participants;
        }
        
        public function addParticipant ($idMembre)
        {
            $this->_participants[] = $idMembre;
        }
	}
