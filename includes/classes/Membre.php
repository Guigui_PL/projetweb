<?php

if ( !defined('Vitoco') ) exit;

	class Membre extends Visiteur
	{
	    use Hashage;
	    private $_idMembre;
		private $_mail;
		private $_mdp;
		private $_nom;
		private $_prenom;
		private $_dateInscription;
		private $_derniereConnexion;
		private $_telephone;
		private $_habite;
		private $_adresse;
		private $_dateNaissance;
		private $_vehicules;
		private $_noteMoyenne;
		private $_nbrNotes;
		private $_banni;

		/**
         * @return mixed
         */
        public function getIdMembre()
        {
            return $this->_idMembre;
        }

        /**
         * @param mixed $_idMembre
         */
        public function setIdMembre($_idMembre)
        {
            $this->_idMembre = $_idMembre;
        }

        /**
         * @return mixed
         */
        public function getAdresse()
        {
            return $this->_adresse;
        }

        /**
         * @param mixed $_adresse
         */
        public function setAdresse(Adresse $_adresse)
        {
            $this->_adresse = $_adresse;
        }

        /**
         * @return mixed
         */
        public function getNoteMoyenne()
        {
            return $this->_noteMoyenne;
        }

        /**
         * @return mixed
         */
        public function getNbrNotes()
        {
            return $this->_nbrNotes;
        }

        /**
         * @return mixed
         */
        public function getBanni ($conversion = null)
        {
            return $this->getDateTime($this->_banni, $conversion);
        }


        /**
         * @param mixed $_noteMoyenne
         */
        public function setNoteMoyenne($_noteMoyenne)
        {
            $this->_noteMoyenne = $_noteMoyenne;
        }

        /**
         * @param mixed $_nbrNotes
         */
        public function setNbrNotes($_nbrNotes)
        {
            $this->_nbrNotes = $_nbrNotes;
        }

        /**
         * @param mixed $_banni
         */
        public function setBanni($_banni)
        {
            $dateTime = $this->creerDateTime($_banni);
            if ($dateTime !== FALSE)
            {
                $this->_banni = $dateTime;
                return true;
            }
            else return false;
        }

        public function setTelephone ($telephone)
		{
			$telephone = trim($telephone);
			$telephone = str_replace(" ", "", $telephone);
			$telephone = str_replace("-", "", $telephone);
			if (preg_match("#(^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^00[0-9]{2}|^0)([0-9]{9}$|[0-9\-\s]{10}$)#", $telephone))
				$this->_telephone = $telephone;
			else
				return false;
		}

		public function setHabite ($idAdresse)
		{
		    $this->_habite = $idAdresse;
		}

		public function setVehicules (array $vehicules)
		{
			$this->_vehicules = $vehicules;
		}

		public function addVehicule (Vehicule $vehicule)
		{
			$this->_vehicules[] = $vehicule;
		}

		public function removeVehicule (Vehicule $vehicule)
		{
			foreach ($this->_vehicules as $key => $value)
			{
				if ($value == $vehicule)
				{
					unset($this->_vehicules[$key]);
					return;
				}
			}
		}

		public function setDateNaissance ($dateNaissance)
		{
			$dateTime = $this->creerDateTime($dateNaissance);
			if ($dateTime !== FALSE)
			{
				$this->_dateNaissance = $dateTime;
				return true;
			}
			else return false;
		}

		public function setMail ($email)
		{
			if (!is_string($email) || filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE)  return false;

			$this->_mail = $email;
			return true;
		}

		public function setMdp  ($mdp, $hasher = false)
		{
			if (!is_string($mdp))
				throw new Exception('Le mot de passe doit être une chaîne de caractères.');

			if ($hasher == true) $this->_mdp = $this->hashage($mdp);
			else $this->_mdp = $mdp;
		}

		public function setNom ($nom)
		{
			if (!is_string($nom) || empty($nom))
				throw new Exception('Le nom doit être une chaîne de caractères.');

			$this->_nom = $nom;
		}

		public function setPrenom ($prenom)
		{
			if (!is_string($prenom) || empty($prenom))
				throw new Exception('Le prenom doit être une chaîne de caractères.');

			$this->_prenom = $prenom;
		}

		public function setDateInscription ($dateInscription)
		{
			$this->_dateInscription = $this->creerDateTime($dateInscription);
		}

		public function setDerniereConnexion ($derniereConnexion)
		{
			$this->_derniereConnexion = $this->creerDateTime($derniereConnexion);
		}

		public function getNomAffiche(Visiteur $utilisateurDemandeur)
		{
			if ($utilisateurDemandeur->getDroit("voirInfosUtilisateurs"))
				return $this->_prenom . ' ' . $this->_nom;
			else
				return $this->_prenom;
		}

		public function estConnecte()
		{
		    $dateActuelle = new DateTime('now');
		    return !empty($this->_dateDernierePageVue)
		      && $this->_dateDernierePageVue < $dateActuelle->sub(new DateInterval('PT15M'));
		}

		public function estBanni()
		{
		    $dateActuelle = new DateTime('now');
		    return !empty($this->_banni)
		    && $this->_banni > $dateActuelle;
		}

		public function getTelephone () { return $this->_telephone; }
		public function getHabite () { return $this->_habite; }
		public function getDateNaissance ($conversion = null)  { return $this->getDateTime($this->_dateNaissance, $conversion); }
		public function getVehicules () { return $this->_vehicules; }
		public function getAge ()
		{
			if ($this->_dateNaissance != null)
			{
				$auJour = new DateTime();
				$age = $auJour->diff($this->_dateNaissance);
				return $age->format("%y");
			}
			else return false;
		}
		public function getMail() { return $this->_mail; }
		public function getMdp() { return $this->_mdp; }
		public function getNom() { return $this->_nom; }
		public function getPrenom() { return $this->_prenom; }
		public function getDateInscription ($conversion = null)  { return $this->getDateTime($this->_dateInscription, $conversion); }
		public function getDerniereConnexion ($conversion = null)  { return $this->getDateTime($this->_derniereConnexion, $conversion); }

		static function __set_state(array $array)
		{
		    $tmp = new Membre();
		    $tmp->_idSession = $array['_idSession'];
		    $tmp->_role = $array['_role'];
		    $tmp->_IP = $array['_IP'];
		    $tmp->_dernierePageVue = $array['_dernierePageVue'];
		    $tmp->_dateDernierePageVue = $array['_dateDernierePageVue'];
		    $tmp->_navigateur = $array['_navigateur'];
		    $tmp->_idMembreConnecte = $array['_idMembreConnecte'];
		    $tmp->_droitsDuVisiteur = $array['_droitsDuVisiteur'];
		    $tmp->_idMembre = $array['_idMembre'];
		    $tmp->_mail = $array['_mail'];
		    $tmp->_mdp = $array['_mdp'];
		    $tmp->_nom = $array['_nom'];
		    $tmp->_prenom = $array['_prenom'];
		    $tmp->_dateInscription = $array['_dateInscription'];
		    $tmp->_derniereConnexion = $array['_derniereConnexion'];
		    $tmp->_telephone = $array['_telephone'];
		    $tmp->_habite = $array['_habite'];
		    $tmp->_adresse = $array['_adresse'];
		    $tmp->_dateNaissance = $array['_dateNaissance'];
		    $tmp->_vehicules = $array['_vehicules'];
		    $tmp->_noteMoyenne = $array['_noteMoyenne'];
		    $tmp->_nbrNotes = $array['_nbrNotes'];
		    $tmp->_banni = $array['_banni'];
		    return $tmp;
		}
	}
