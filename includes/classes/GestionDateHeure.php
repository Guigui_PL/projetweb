<?php

if ( !defined('Vitoco') ) exit;

	trait GestionDateHeure
	{
		private $_defaultTimeZone = 'Europe/Paris';
		private $_traitTimeZone;
		
		public function setTraitTimeZone ($timeZone = null)
		{
			if ($timeZone == null || !in_array($timeZone, DateTimeZone::listIdentifiers()))  $this->_traitTimeZone = new DateTimeZone($this->_defaultTimeZone);
			else  $this->_traitTimeZone = new DateTimeZone($timeZone);
		}
		
		public function getDateTimeNow ($timeZone = null)
		{
			$this->setTraitTimeZone($timeZone);
			return new DateTime("now", $this->_traitTimeZone);
		}
		
		public function creerDateTime ($dateTime, $timeZone = null)
		{
			$this->setTraitTimeZone($timeZone);
			if ($dateTime == "0000-00-00") $dateTime = "0000-01-01";
			else if ($dateTime == "0000-00-00 00:00:00") $dateTime = "0000-01-01 00:00:00";
			else if ($dateTime == null) $dateTime = "0000-01-01";
			if (mb_substr_count($dateTime, ":") == 1) $dateTime .= ":00";
			if (mb_substr_count($dateTime, "/") > 0) $dateTime = str_replace("/", "-", $dateTime);
			
			return new DateTime($dateTime, $this->_traitTimeZone);
		}
		
		public function getDateTime ($dateTime, $conversion = null) // Renvoie la date et l'heure avec le fuseau du membre si connecté
		{
			if ($dateTime != null)
			{
				if (isset($conversion))
				{
					if ($conversion == 'int')
					{
						if (!isset($_SESSION['membre'])) $this->setTraitTimeZone($this->_defaultTimeZone);
						else $this->setTraitTimeZone($_SESSION['membre']->getTimeZone());
						
						$formatter = new IntlDateFormatter('fr_FR',IntlDateFormatter::LONG,
							IntlDateFormatter::NONE,
							$this->_traitTimeZone,
							IntlDateFormatter::GREGORIAN );
						
						return $formatter->format($dateTime);
					}
					else if ($conversion == 'intWithHour')
					{
						if (!isset($_SESSION['membre'])) $this->setTraitTimeZone($this->_defaultTimeZone);
						else $this->setTraitTimeZone($_SESSION['membre']->getTimeZone());
						
						$formatter = new IntlDateFormatter('fr_FR',IntlDateFormatter::FULL,
							IntlDateFormatter::SHORT,
							$this->_traitTimeZone,
							IntlDateFormatter::GREGORIAN );
						
						return $formatter->format($dateTime);
					}
					else if ($conversion == MYSQL_DATETIME_FORMAT || $conversion == MYSQL_DATE_FORMAT) 
						return $dateTime->format($conversion);
					else
					{
						if (!isset($_SESSION['membre'])) $this->setTraitTimeZone($this->_defaultTimeZone);
						else $this->setTraitTimeZone($_SESSION['membre']->getTimeZone());
						$dateTime->setTimezone($this->_traitTimeZone);
						
						return $dateTime->format($conversion);
					}
				}
				else
				{
					if (!isset($_SESSION['membre'])) $this->setTraitTimeZone($this->_defaultTimeZone);
					else $this->setTraitTimeZone($_SESSION['membre']->getTimeZone());
					return $dateTime->setTimezone($this->_traitTimeZone);
				}
			}
			else return null;
		}
		
		/**
		 * Méthode listeTimezones
		 * Retourne une balise HTML select contenant tous les fuseaux horaires
		 * @param string $timeZone 
		 * @return string
		 */
		
		public static function listeTimeZones ($timeZone) 
		{
			$listeTimezones = timezone_identifiers_list();
			$listeFuseau = array();
			$villes = array();
			
			foreach($listeTimezones as $tz) 
			{
				$spos = strpos($tz, '/');
				if ($spos !== FALSE) 
				{
					$continent = t(substr($tz, 0, $spos));
					$ville = substr($tz, $spos+1);
					$listeFuseau[$continent][] = array('tz_name' => $tz, 'city' => $ville);
				}
				
				if ($tz == 'UTC')
				{
					$listeFuseau['UTC'][] = array('tz_name' => 'UTC', 'city' => 'UTC');
				}
			}
			
			$select = '<select id="timeZone" name="timeZone">' ;
			
			foreach ($listeFuseau as $continent => $zone) 
			{
				$select .= '<optgroup label="'.ucfirst(strtolower($continent)).'">';
				foreach ($zone as $fuseau) 
				{
					$select .= '<option value="'.htmlentities($fuseau['tz_name']).'"';
					
					if ($fuseau['tz_name'] == $timeZone) $select .= ' selected="selected" ';
					
					$timeoffset = date_offset_get(date_create('now', timezone_open($fuseau['tz_name'])) );
					$selectated_toffset = '(UTC'.(($timeoffset < 0) ? '–' : '+').str_pad(floor((abs($timeoffset) / 3600)), 2, '0', STR_PAD_LEFT).':'.str_pad( floor((abs($timeoffset) % 3600) / 60), 2, '0', STR_PAD_LEFT).') ';
					$select .= '>'.$selectated_toffset.' '.htmlentities($fuseau['city']).'</option>';
				}
				$select .= '</optgroup>';
			}
			$select .= '</select>';
			return $select;
		}
		
		/**
		 * Méthode estValideDateTime ()
		 * Vérifie si une chaîne de caractères est valide pour la création d'un objet dateTime
		 * @param string $dateTime
		 * @param string $dateFormat
		 * @return bool
		 */
		 
		function estValideDateTime ($dateTime, $dateFormat) 
		{
			$date = DateTime::createFromFormat($dateFormat, $dateTime, $this->_traitTimeZone);
			
			return $date && DateTime::getLastErrors()["warning_count"] == 0 && DateTime::getLastErrors()["error_count"] == 0;
		}
	}