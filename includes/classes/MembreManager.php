<?php

class MembreManager
{
    use ToolsForManagers, Hashage, GestionDateHeure;
    private $_adresseManager;
    private $_vehiculeManager;
    
    public function __construct ($bdd)
    {
        $this->setBdd($bdd);
        $this->_adresseManager = new AdresseManager($bdd);
        $this->_vehiculeManager = new VehiculeManager($bdd);
    }
    
    public function getListe ($limit, $listeParametres = null, $ordre = null)
    {
        if ($listeParametres != null || $ordre != null)
            $champs = $this->listeColonnes(['Membre', 'Visiteur']);
            else $champs = null;
            
            $requete = "SELECT * FROM Membre
                         LEFT JOIN Visiteur ON idMembre = idMembreConnecte ";
            $req = $this->executeRequeteListe($requete, $champs, $listeParametres, $ordre, $limit);
            
            return $this->genereListe($req, 'Membre');
    }
    
    public function getMembre (array $listeParametres)
    {
        if ($listeParametres != null || $ordre != null)
            $champs = $this->listeColonnes(['Membre', 'Visiteur']);
        else $champs = null;
            
        $requete = "SELECT * FROM Membre
                     LEFT JOIN Visiteur ON idMembre = idMembreConnecte ";
        $req = $this->executeRequeteListe($requete, $champs, $listeParametres, null, ' LIMIT 0, 1');
        
        $resultat = $this->genereListe($req, 'Membre');
        $membre = reset($resultat);
        return $membre;
    }
    
    public function getNombre ()
    {
        $req = $this->_bdd->query("SELECT valeur AS count FROM Statistiques WHERE nom = 'nbrMembres'");
        
        return $req->fetch(PDO::FETCH_ASSOC)['count'];
    }
    
    public function loadVehicules (array $listeMembres)
    {
        foreach ($listeMembres as &$membre)
        {
            $membre->setVehicules($this->_vehiculeManager->getListe('', ['idPossesseur', DB::EGAL, $membre->getIdMembre()]));
        }
    }
    
    public function loadAdresses (array $listeMembres)
    {
        foreach ($listeMembres as &$membre)
        {
            $membre->setAdresse($this->_adresseManager->getAdresse(['idAdresse', DB::EGAL, $membre->getIdMembre()]));
        }
    }
    
    public function delete ($id)
    {
        $req = $this->_bdd->prepare('DELETE FROM Membre WHERE idMembre = :id');
        return $req->execute(array('id' => $id));
    }
    
    public function update (Membre $membre)
    {
        $req = $this->_bdd->prepare('UPDATE Membre SET mail = :mail, mdp = :mdp, role = :role, nom = :nom, prenom = :prenom, dateNaissance = :dateNaissance, habite = :habite, telephone = :telephone, dateInscription = :dateInscription, derniereConnexion = :derniereConnexion, banni = :banni WHERE idMembre = :id');
        $req->bindValue(":mail", $membre->getMail(), PDO::PARAM_STR);
        $req->bindValue(":mdp", $membre->getMdp(), PDO::PARAM_STR);
        $req->bindValue(":role", $membre->getRole(), PDO::PARAM_STR);
        $req->bindValue(":nom", $membre->getNom(), PDO::PARAM_STR);
        $req->bindValue(":prenom", $membre->getPrenom(), PDO::PARAM_STR);
        $req->bindValue(":dateNaissance", $membre->getDateNaissance(MYSQL_DATE_FORMAT), PDO::PARAM_STR);
        $req->bindValue(":habite", $membre->getHabite(), PDO::PARAM_STR);
        $req->bindValue(":telephone", $membre->getTelephone(), PDO::PARAM_STR);
        $req->bindValue(":dateInscription", $membre->getDateInscription(MYSQL_DATE_FORMAT), PDO::PARAM_STR);
        $req->bindValue(":derniereConnexion", $membre->getDerniereConnexion(MYSQL_DATETIME_FORMAT), PDO::PARAM_STR);
        $req->bindValue(":banni", $membre->getBanni(MYSQL_DATETIME_FORMAT), PDO::PARAM_STR);
        $req->bindValue(":id", $membre->getIdMembre(), PDO::PARAM_STR);
        return $req->execute();
    }
    
    public function insert (Membre $membre)
    {
        $req = $this->_bdd->prepare('INSERT INTO Membre(mail, mdp, role, nom, prenom, dateNaissance, habite, telephone, dateInscription) VALUES (:mail, :mdp, :role, :nom, :prenom, :dateNaissance, :habite, :telephone, NOW())');
        $req->bindValue(":mail", $membre->getMail(), PDO::PARAM_STR);
        $req->bindValue(":mdp", $membre->getMdp(), PDO::PARAM_STR);
        $req->bindValue(":role", $membre->getRole(), PDO::PARAM_STR);
        $req->bindValue(":nom", $membre->getNom(), PDO::PARAM_STR);
        $req->bindValue(":prenom", $membre->getPrenom(), PDO::PARAM_STR);
        $req->bindValue(":dateNaissance", $membre->getDateNaissance(MYSQL_DATE_FORMAT), PDO::PARAM_STR);
        $req->bindValue(":habite", $membre->getHabite(), PDO::PARAM_STR);
        $req->bindValue(":telephone", $membre->getTelephone(), PDO::PARAM_STR);
        return $req->execute();
    }
    
    public function existeMail ($mail)
    {
        $req = $this->_bdd->prepare("SELECT COUNT(*) AS count FROM Membre WHERE mail = :mail");
        $req->execute(array('mail' => $mail));
        
        return $req->fetch(PDO::FETCH_ASSOC)['count'] > 0;
    }
    
    public function connecte ($mail, $mdp)
    {
        if ($this->existeMail($mail))
        {
            $membre = $this->getMembre(['mail', DB::EGAL, $mail]);
            
            if (!empty($membre->getIdMembre()) 
                && $this->verificationHashage($mdp, $membre->getMdp())
                && !$membre->estBanni())
            {
                $visiteurManager = new VisiteurManager($this->_bdd);
                $visiteurManager->delete(session_id());
                $_SESSION = array();
                session_destroy();
                session_start();
                $visiteurManager->updateVisiteurs();
                $_SESSION['visiteur']->setIdMembreConnecte($membre->getIdMembre());
                $visiteurManager->updateVisiteur($_SESSION['visiteur']);
                
                $membre = $this->getMembre(['mail', DB::EGAL, $mail]);
                
                $_SESSION['visiteur'] = $membre;
                $_SESSION['visiteur']->setDerniereConnexion($this->getDateTimeNow()->format(MYSQL_DATETIME_FORMAT));
                
                $this->update($_SESSION['visiteur']);
                
                return true;
            }
            else
            {
                if (!$this->verificationHashage($mdp, $membre->getMdp()))
                    throw new Exception("Mot de passe invalide");
                if ($membre->getBanni() > $this->getDateTimeNow())
                    throw new Exception('Vous êtes banni pendant encore '.$TempsRestant->y.' année(s) '.$TempsRestant->m.' mois '.$TempsRestant->d.' jour(s) '.$TempsRestant->h.' heure(s) '.$TempsRestant->i.' minute(s) et '.$TempsRestant->s.' seconde(s).');
            }
        }
        else 
        {
            throw new Exception("Ce mail n'existe pas !");
        }
    }
    
    public function deconnecte()
    {
        if ($_SESSION['visiteur']->estConnecte())
        {
            $visiteurManager = new VisiteurManager($this->_bdd);
            $visiteurManager->delete(session_id());
            $_SESSION = array();
            session_destroy();
            session_start();
            $visiteurManager->updateVisiteurs();            
        }
    }
}

