<?php

    if ( !defined('Vitoco') ) exit;

	class Appreciation extends Message
	{
		private $_idAppreciation;
		private $_note;
        /**
         * @return mixed
         */
        public function getIdAppreciation()
        {
            return $this->_idAppreciation;
        }
    
        /**
         * @return mixed
         */
        public function getNote()
        {
            return $this->_note;
        }
    
        /**
         * @param mixed $_idAppreciation
         */
        public function setIdAppreciation($_idAppreciation)
        {
            $this->_idAppreciation = $_idAppreciation;
        }
    
        /**
         * @param mixed $_note
         */
        public function setNote($_note)
        {
            $this->_note = $_note;
        }
    
		
	} 
 
