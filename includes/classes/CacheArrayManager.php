<?php

if ( !defined('Vitoco') ) exit;
	
	class CacheArrayManager extends CacheManager
	{
		private $_debut; // Variable contenant ce qu'il y a à écrire au début du fichier du genre <?php $array = ....
		
		public function __construct ()
		{
			parent::__construct();
			$this->setDebut('<?php if ( !defined(\'Vitoco\') ) exit; $contenu = ');
		}
		
		public function readCache (Cache $objetCache)
		{
			$this->setExpire(time()-$objetCache->getDuree());
			$this->setChemin($objetCache->getNom().'.php');
			
			if(file_exists($this->_chemin) && (filemtime($this->_chemin) > $this->_expire || $objetCache->getDuree() == 0) )
			{
				require_once($this->_chemin);
				$objetCache->setContenu($contenu);
			}
			else
				return false;
		}
		
		public function writeCache (Cache $objetCache)
		{
			$this->setChemin($objetCache->getNom().'.php');
			file_put_contents($this->_chemin, $this->_debut.var_export($objetCache->getContenu(), true).'?>');
		}
		
		public function videCache (Cache $objetCache)
		{
			$this->setChemin($objetCache->getNom().'.php');
			if (file_exists($this->_chemin)) unlink($this->_chemin);
		}
		
		public function setDebut ($debut)
		{
			if (!is_string($debut)) 
			{
				trigger_error('Le début du fichier à écrire doit être une chaine de caractère.', E_USER_WARNING);
				return;
			}
			$this->_debut = $debut;
		}
	}