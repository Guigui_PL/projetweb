<?php

class VehiculeManager
{
    use ToolsForManagers;
    
    public function __construct ($bdd)
    {
        $this->setBdd($bdd);
    }
    
    public function getListe ($limit, $listeParametres = null, $ordre = null)
    {
        if ($listeParametres != null || $ordre != null)
            $champs = $this->listeColonnes(['Vehicule']);
            else $champs = null;
            
            $requete = "SELECT * FROM Vehicule ";
            $req = $this->executeRequeteListe($requete, $champs, $listeParametres, $ordre, $limit);
            
            return $this->genereListe($req, 'Vehicule');
    }
    
    public function getVehicule (array $listeParametres)
    {
        if ($listeParametres != null || $ordre != null)
            $champs = $this->listeColonnes(['Vehicule']);
            else $champs = null;
            
            $requete = "SELECT * FROM Vehicule ";
            $req = $this->executeRequeteListe($requete, $champs, $listeParametres, null, ' LIMIT 0, 1');
            
            return $this->genereListe($req, 'Vehicule')[0];
    }
    
    public function getNombre ()
    {
        $req = $this->_bdd->query("SELECT COUNT(*) AS count FROM Vehicule");
        
        return $req->fetch(PDO::FETCH_ASSOC)['count'];
    }
    
    public function delete ($id)
    {
        $req = $this->_bdd->prepare('DELETE FROM Vehicule WHERE idVehicule = :id');
        return $req->execute(array('id' => $id));
    }
    
    public function update (Vehicule $vehicule)
    {
        $req = $this->_bdd->prepare('UPDATE Vehicule SET idPossesseur = :idPossesseur, marque = :marque, modele = :modele, annee = :annee, consommation = :consommation, nbrPlacesPassagers = :nbrPlacesPassagers WHERE idVehicule = :id');
        $req->bindValue(":idPossesseur", $vehicule->getIdPossesseur(), PDO::PARAM_STR);
        $req->bindValue(":marque", $vehicule->getMarque(), PDO::PARAM_STR);
        $req->bindValue(":modele", $vehicule->getModele(), PDO::PARAM_STR);
        $req->bindValue(":annee", $vehicule->getAnnee(), PDO::PARAM_STR);
        $req->bindValue(":consommation", $vehicule->getConsommation(), PDO::PARAM_STR);
        $req->bindValue(":nbrPlacesPassagers", $vehicule->getNbrPlacesPassagers(), PDO::PARAM_STR);
        $req->bindValue(":id", $vehicule->getIdVehicule(), PDO::PARAM_STR);
        return $req->execute();
    }
    
    public function insert (Vehicule $vehicule)
    {
        $req = $this->_bdd->prepare('INSERT INTO Vehicule(idPossesseur, marque, modele, annee, consommation, nbrPlacesPassagers) VALUES (:idPossesseur, :marque, :modele, :annee, :consommation, :nbrPlacesPassagers)');
        $req->bindValue(":idPossesseur", $vehicule->getIdPossesseur(), PDO::PARAM_STR);
        $req->bindValue(":marque", $vehicule->getMarque(), PDO::PARAM_STR);
        $req->bindValue(":modele", $vehicule->getModele(), PDO::PARAM_STR);
        $req->bindValue(":annee", $vehicule->getAnnee(), PDO::PARAM_STR);
        $req->bindValue(":consommation", $vehicule->getConsommation(), PDO::PARAM_STR);
        $req->bindValue(":nbrPlacesPassagers", $vehicule->getNbrPlacesPassagers(), PDO::PARAM_STR);
        return $req->execute();
    }
}

