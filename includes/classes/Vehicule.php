<?php

if ( !defined('Vitoco') ) exit;

	class Vehicule
	{
	    use GestionDateHeure, Hydrate;
	    
		private $_idVehicule;
		private $_idPossesseur;
		private $_marque;
		private $_modele;
		private $_annee;
		private $_consommation;
		private $_nbrPlacesPassagers;
        /**
         * @return mixed
         */
        public function getIdVehicule()
        {
            return $this->_idVehicule;
        }
    
        /**
         * @return mixed
         */
        public function getIdPossesseur()
        {
            return $this->_idPossesseur;
        }
    
        /**
         * @return mixed
         */
        public function getMarque()
        {
            return $this->_marque;
        }
    
        /**
         * @return mixed
         */
        public function getModele()
        {
            return $this->_modele;
        }
    
        /**
         * @return mixed
         */
        public function getAnnee()  
        { 
            $this->_annee;
        }
    
        /**
         * @return mixed
         */
        public function getConsommation()
        {
            return $this->_consommation;
        }
    
        /**
         * @return mixed
         */
        public function getNbrPlacesPassagers()
        {
            return $this->_nbrPlacesPassagers;
        }
    
        /**
         * @param mixed $_idVehicule
         */
        public function setIdVehicule($_idVehicule)
        {
            $this->_idVehicule = $_idVehicule;
        }
    
        /**
         * @param mixed $_idPossesseur
         */
        public function setIdPossesseur($_idPossesseur)
        {
            $this->_idPossesseur = $_idPossesseur;
        }
    
        /**
         * @param mixed $_marque
         */
        public function setMarque($_marque)
        {
            $this->_marque = $_marque;
        }
    
        /**
         * @param mixed $_modele
         */
        public function setModele($_modele)
        {
            $this->_modele = $_modele;
        }
    
        /**
         * @param mixed $_annee
         */
        public function setAnnee($_annee)
        {
            $this->_annee = $_annee;
        }
    
        /**
         * @param mixed $_consommation
         */
        public function setConsommation($_consommation)
        {
            $this->_consommation = $_consommation;
        }
    
        /**
         * @param mixed $_nbrPlacesPassagers
         */
        public function setNbrPlacesPassagers($_nbrPlacesPassagers)
        {
            $this->_nbrPlacesPassagers = $_nbrPlacesPassagers;
        }
    
		
		
	} 
 
