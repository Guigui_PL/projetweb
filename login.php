<?php

    define('Vitoco', true);
    $titre = 'Vitoco - Connexion';
    $cache = false;

    require_once('includes/init.php');
    require_once('includes/head.php');
    require_once('includes/popup.php');

    $tpl = new Smarty;

    $dateJour = (new DateTime("now"));
    $dateMinus18 = $dateJour->sub(new DateInterval("P18Y"));
    $fmtDateJour = $dateJour->format("Y-m-d");
    $fmtDateMinus18 = $dateMinus18->format("Y-m-d");

    $tpl->assign(array(
      'dateMinus18' => $fmtDateMinus18
  		));

    // Si on rempli un formulaire de login.html
    if ($_SERVER['REQUEST_METHOD'] == "POST") {

      $membreManager = new MembreManager($bdd);

      // Inscription
      if (isset($_POST['pwd-check'])) {
        try {
          if ($_POST["pwd-check"] == $_POST["pwd"]
            && !empty($_POST["pwd-check"])
            && !empty($_POST["prenom"])
            && !empty($_POST["nom"])
            && !empty($_POST["naissance"])
            && !empty($_POST["mail"])
            && !empty($_POST["ville"]))
          {
            $membre = new Membre();
            $membre->setPrenom($_POST['prenom']);
            $membre->setNom($_POST['nom']);
            $membre->setDateNaissance($_POST['naissance']);
            if (!empty($_POST["tel"]))
              $membre->setTelephone($_POST['tel']);
            $membre->setMail($_POST['mail']);
            $membre->setMdp($_POST['pwd'], true);

            $adresse = new Adresse();
            $adresse->setVille($_POST['ville']);
            if (!empty($_POST["nom-voie"]))
              $adresse->setNomVoie($_POST['nom-voie']);
            if (!empty($_POST["num-voie"]))
              $adresse->setNumeroVoie($_POST['num-voie']);
            if (!empty($_POST["complement"]))
              $adresse->setComplement($_POST['complement']);
            if (!empty($_POST["CP"]))
              $adresse->setCP($_POST['CP']);

            $adresseManager = new AdresseManager($bdd);

            $idAdresse = $adresseManager->insert($adresse);
            if ($idAdresse !== false)
            {
              $membre->setHabite($idAdresse);
              $reussi = $membreManager->insert($membre);
              if ($reussi === false)
                throw new Exception("Echec de l'enregistrement du membre dans la base de donnée.");
            }
            else
              throw new Exception("Echec de l'enregistrement de l'adresse dans la base de donnée.");

            $membreManager->connecte($_POST['mail'], $_POST["pwd"]);

            $_SESSION['popup_type'] = "notification";
            $_SESSION['popup_content'] = "Votre inscription s'est déroulée avec succès. Vous êtes maintenant connecté.";
            header('Location: index.php');
          }
          else
            if (!($_POST["pwd-check"] == $_POST["pwd"]))
              throw new Exception("Les mots de passe ne correspondent pas.");
        }
        catch (Exception $e) {
          $_SESSION['popup_type'] = "error";
          $_SESSION['popup_content'] = "L'inscription a échoué. Veuillez réessayer. ".$e->getMessage();
          header('Location: login.php');
        }
      }

      // Connexion
      else {
        try {
          $membreManager->connecte($_POST['mail'], $_POST["pwd"]);
          header('Location: index.php');
        }
        catch (Exception $e) {
          $_SESSION['popup_type'] = "error";
          $_SESSION['popup_content'] = "La connexion a échoué. Veuillez réessayer. <br />".$e->getMessage();
          header('Location: login.php');
        }
      }
    }

    // Si on veut se connecter ou s'inscrire
    else {
      if ($_SESSION['visiteur']->estConnecte()) {
        $membreManager = new MembreManager($bdd);
        $membreManager->deconnecte();
        $_SESSION['popup_type'] = "notification";
        $_SESSION['popup_content'] = "Vous avez bien été déconnecté.";
        header('Location: index.php');
      }
      else $tpl->display('specific/login.html');
    }

    require_once('includes/footer.php');
