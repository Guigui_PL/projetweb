CREATE DATABASE IF NOT EXISTS `SANS_TITRE` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `SANS_TITRE`;

CREATE TABLE `VISITEUR` (
  `id_de_session` VARCHAR(42),
  `ip` VARCHAR(42),
  `dernière_page_vue` VARCHAR(42),
  `date_dernière_page_vue` VARCHAR(42),
  `navigateur` VARCHAR(42),
  `id_membre` VARCHAR(42),
  PRIMARY KEY (`id_de_session`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ADRESSE` (
  `id_adresse` VARCHAR(42),
  `numéro_de_voie` VARCHAR(42),
  `nom_de_la_voie` VARCHAR(42),
  `complément` VARCHAR(42),
  `cp` VARCHAR(42),
  `ville` VARCHAR(42),
  PRIMARY KEY (`id_adresse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `PASSE_PAR` (
  `id_trajet_disponible` VARCHAR(42),
  `id_adresse` VARCHAR(42),
  `ordre` VARCHAR(42),
  PRIMARY KEY (`id_trajet_disponible`, `id_adresse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `VÉHICULE` (
  `id_véhicule` VARCHAR(42),
  `marque` VARCHAR(42),
  `modèle` VARCHAR(42),
  `année` VARCHAR(42),
  `consommation` VARCHAR(42),
  `nombre_de_places_passagers` VARCHAR(42),
  `id_membre` VARCHAR(42),
  PRIMARY KEY (`id_véhicule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `MEMBRE` (
  `id_membre` VARCHAR(42),
  `prénom` VARCHAR(42),
  `nom` VARCHAR(42),
  `date_de_naissance` VARCHAR(42),
  `téléphone` VARCHAR(42),
  `email` VARCHAR(42),
  `mot_de_passe` VARCHAR(42),
  `note_moyenne` VARCHAR(42),
  `nombre_de_notes` VARCHAR(42),
  `date_d'inscription` VARCHAR(42),
  `date_de_dernière_connexion` VARCHAR(42),
  `rôle` VARCHAR(42),
  `banni` VARCHAR(42),
  `id_adresse` VARCHAR(42),
  PRIMARY KEY (`id_membre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `PARTICIPE` (
  `id_membre` VARCHAR(42),
  `id_trajet_disponible` VARCHAR(42),
  PRIMARY KEY (`id_membre`, `id_trajet_disponible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `TRAJET_DISPONIBLE` (
  `id_trajet_disponible` VARCHAR(42),
  `nombre_de_places_passager` VARCHAR(42),
  `distance` VARCHAR(42),
  `date` VARCHAR(42),
  `date_d'arrivée_estimée` VARCHAR(42),
  `durée_estimée` VARCHAR(42),
  `prix_au_kilomètre` VARCHAR(42),
  `description` VARCHAR(42),
  `cigarette_autorisée` VARCHAR(42),
  `bagage_autorisée` VARCHAR(42),
  `animal_autorisé` VARCHAR(42),
  `date_de_création` VARCHAR(42),
  `id_membre` VARCHAR(42),
  `id_trajet_type` VARCHAR(42),
  `id_véhicule` VARCHAR(42),
  PRIMARY KEY (`id_trajet_disponible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `TRAJET_TYPE` (
  `id_trajet_type` VARCHAR(42),
  `ville_de_départ` VARCHAR(42),
  `ville_d'arrivée` VARCHAR(42),
  `distance` VARCHAR(42),
  `temps_indicatif` VARCHAR(42),
  `plafond_du_prix_au_kilomètre` VARCHAR(42),
  PRIMARY KEY (`id_trajet_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `MESSAGE` (
  `id_message` VARCHAR(42),
  `corps` VARCHAR(42),
  `id_membre` VARCHAR(42),
  `id_trajet_disponible` VARCHAR(42),
  PRIMARY KEY (`id_message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `APPRÉCIATION` (
  `id_appréciation` VARCHAR(42),
  `corps` VARCHAR(42),
  `note` VARCHAR(42),
  `id_membre` VARCHAR(42),
  `id_trajet_disponible` VARCHAR(42),
  PRIMARY KEY (`id_appréciation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `VISITEUR` ADD FOREIGN KEY (`id_membre`) REFERENCES `MEMBRE` (`id_membre`);
ALTER TABLE `PASSE_PAR` ADD FOREIGN KEY (`id_adresse`) REFERENCES `ADRESSE` (`id_adresse`);
ALTER TABLE `VÉHICULE` ADD FOREIGN KEY (`id_membre`) REFERENCES `MEMBRE` (`id_membre`);
ALTER TABLE `MEMBRE` ADD FOREIGN KEY (`id_adresse`) REFERENCES `ADRESSE` (`id_adresse`);
ALTER TABLE `PARTICIPE` ADD FOREIGN KEY (`id_membre`) REFERENCES `MEMBRE` (`id_membre`);
ALTER TABLE `TRAJET_DISPONIBLE` ADD FOREIGN KEY (`id_véhicule`) REFERENCES `VÉHICULE` (`id_véhicule`);
ALTER TABLE `TRAJET_DISPONIBLE` ADD FOREIGN KEY (`id_membre`) REFERENCES `MEMBRE` (`id_membre`);
ALTER TABLE `MESSAGE` ADD FOREIGN KEY (`id_membre`) REFERENCES `MEMBRE` (`id_membre`);
ALTER TABLE `APPRÉCIATION` ADD FOREIGN KEY (`id_membre`) REFERENCES `MEMBRE` (`id_membre`);