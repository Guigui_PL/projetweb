<?php

    define('Vitoco', true);
    $titre = 'Détails du trajet';
    $cache = false;

    require_once('includes/init.php');
    require_once('includes/head.php');
    require_once('includes/popup.php');

    $tpl = new Smarty;

    if ($_SERVER['REQUEST_METHOD'] == "POST") {
      if (!empty($_POST['corps'])) {
        try {
          $messageManager = new MessageManager($bdd);
          $message = new Message;
          $message->setCorps($_POST['corps']);
          $message->setIdTrajet($_GET['t']);
          $message->setIdAuteur($_SESSION['visiteur']->getIdMembreConnecte());
          $messageManager->insert($message);
        }
        catch (Exception $e) {
          $_SESSION['popup_type'] = "error";
          $_SESSION['popup_content'] = "Une erreur est survenue lors de l'enregistrement de votre message. Veuillez réessayer.<br>".$e->getMessage();
          header('Location: trip.php?t='.$_GET['t']);
        }
      }
      else {
        $_SESSION['popup_type'] = "error";
        $_SESSION['popup_content'] = "Vous ne pouvez pas envoyer de message vide.";
        header('Location: trip.php?t='.$_GET['t']);
      }
    }
    else if (isset($_GET['signin'])) {
      try {
        $trajetManager = new TrajetDisponibleManager($bdd);
        $trajet = $trajetManager->getTrajetDisponible(['idTrajetDisponible', DB::EGAL, $_GET['t']]);
        $vehiculeManager = new VehiculeManager($bdd);
        $vehicule = $vehiculeManager->getVehicule(['idVehicule', DB::EGAL, $trajet->getVehicule()]);
        if (!$trajet->getEffectue() && count($trajet->getParticipants()) < $vehicule->getNbrPlacesPassagers()) {
          $trajet->addParticipant($_SESSION['visiteur']->getIdMembre());
          $trajetManager->update($trajet);
          $_SESSION['popup_type'] = "notification";
          $_SESSION['popup_content'] = "Vous venez de vous inscrire à ce trajet.";
          header('Location: trip.php?t='.$_GET['t']);
        }
        else {
          $_SESSION['popup_type'] = "error";
          $_SESSION['popup_content'] = "Impossible de vous inscrire à ce trajet.";
          header('Location: trip.php?t='.$_GET['t']);
        }
      }
      catch (Exception $e) {
        $_SESSION['popup_type'] = "error";
        $_SESSION['popup_content'] = "Impossible de vous inscrire à ce trajet.<br>".$e->getMessage();
        header('Location: trip.php?t='.$_GET['t']);
      }
    }

    if (!empty($_GET['t'])) {
      if ($_SESSION['visiteur']->estConnecte()) {
        $trajetManager = new TrajetDisponibleManager($bdd);
        $trajet = new TrajetDisponible;
        try {
          $trajet = $trajetManager->getTrajetDisponible(['idTrajetDisponible', DB::EGAL, $_GET['t']]);
        }
        catch (Exception $e) {
          $_SESSION['popup_type'] = "error";
          $_SESSION['popup_content'] = "Le trajet que vous essayez d'atteindre n'existe pas.<br>".$e->getMessage();
          header('Location: index.php');
        }

        $membreManager = new MembreManager($bdd);
        $vehiculeManager = new VehiculeManager($bdd);
        $messageManager = new MessageManager($bdd);
        $conducteur = $membreManager->getMembre(['idMembre', DB::EGAL, $trajet->getConducteur()]);
        $vehicule = $vehiculeManager->getVehicule(['idVehicule', DB::EGAL, $trajet->getVehicule()]);
        $messages = $messageManager->getListe("LIMIT 0, 100", ['idTrajet', DB::EGAL, $_GET['t']]);

        $tpl->assign(array(
          'villeDepart' => $trajet->getVilleDepart()->getVille(),
          'villeArrivee' => $trajet->getVilleArrivee()->getVille(),
          'conducteurPrenom' => $conducteur->getPrenom(),
          'conducteurNom' => $conducteur->getNom(),
          'conducteurAge' => $conducteur->getAge(),
          'conducteurNote' => $conducteur->getNoteMoyenne(),
          'conducteurNbrNotes' => $conducteur->getNbrNotes(),
          'conducteurId' => $conducteur->getIdMembre(),
          'vehiculeMarque' => $vehicule->getMarque(),
          'vehiculeModele' => $vehicule->getModele(),
          'vehiculeAnnee' => $vehicule->getAnnee(),
          'vehiculeConsommation' => $vehicule->getConsommation(),
          'dateDepart' => $trajet->getDateTrajet("d/m/Y"),
          'heureDepart' => $trajet->getDateTrajet("H:i"),
          'dureeEstimee' => $trajet->getDureeEstimee(),
          'distanceEstimee' => $trajet->getDistance(),
          'animalAutorise' => $trajet->getAnimalAutorise(),
          'fumerAutorise' => $trajet->getCigaretteAutorisee(),
          'tailleBagage' => $trajet->getBagagesAutorises(),
          'etapes' => $trajet->getEtapes(),
          'membreManager' => $membreManager,
          'trajetId' => $_GET['t'],
          'messages' => $messages,
          'placesDisponibles' => count($trajet->getParticipants()) < $vehicule->getNbrPlacesPassagers(),
          'trajetTermine' => $trajet->getEffectue(),
          'visiteurId' => $_SESSION['visiteur']->getIdMembre()
        ));

        $tpl->display('specific/trip.html');

      }
      else {
        $_SESSION['popup_type'] = "error";
        $_SESSION['popup_content'] = "Vous devez être connecté pour pouvoir accéder à cette page.";
        header('Location: index.php');
      }
    }
    else {
      $_SESSION['popup_type'] = "error";
      $_SESSION['popup_content'] = "L'adresse à laquelle vous essayez d'accéder est mal formée.";
      header('Location: index.php');
    }

    require_once('includes/footer.php');
