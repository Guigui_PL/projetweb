<?php

    define('Vitoco', true);
    $titre = 'Panneau d\'administration';
    $cache = false;

    require_once('includes/init.php');
    require_once('includes/head.php');
    require_once('includes/popup.php');

    $tpl = new Smarty;

    // Si on est connecté en tant qu'admin
    if ($_SESSION['visiteur']->getRole() === Visiteur::ROLE_ADMIN) {

      // Traitement des formulaires
      if ($_SERVER['REQUEST_METHOD'] == "POST") {

        // Changement de droit / bannissement d'un utilisateur
        if ($_GET['t'] == "users" && isset($_GET['change'])) {
          /*$membreManager = new MembreManager($bdd);

          foreach ($_POST['promouvoir'] as $idMembreAPromouvoir) {
            $membreAPromouvoir = $membreManager->getMembre($idMembreAPromouvoir);
          }*/
        }

        // Ajout d'un trajet type
        else if ($_GET['t'] == "tt" && !empty($_POST['villeDepart'])) {
          try {
            $trajetTypeManager = new TrajetTypeManager($bdd);
            $trajetType = new TrajetType;
            $trajetType->setPlafondPrixKm($_POST['prixKm']);
            $trajetType->setDistance($_POST['distance']);
            $trajetType->setTempsIndicatif($_POST['temps']);

            $adresseManager = new AdresseManager($bdd);
            $adresseDepart = new Adresse;
            $adresseDepart->setVille($_POST['villeDepart']);
            $adresseManager->insert($adresseDepart);
            $adresseArrivee = new Adresse;
            $adresseArrivee->setVille($_POST['villeArrivee']);
            $adresseManager->insert($adresseArrivee);
            $trajetType->setVilleDepart($adresseDepart);
            $trajetType->setVilleArrivee($adresseArrivee);
            $trajetTypeManager->insert($trajetType);
          }
          catch (Exception $e) {
            $_SESSION['popup_type'] = "error";
            $_SESSION['popup_content'] = "Une erreur est survenue lors de la création du trajet type. Veuillez réessayer.<br>".$e->getMessage();
            header('Location: admin.php?t=tt');
          }
        }

        // Suppression d'un trajet type
        else if ($_GET['t'] == "tt" && isset($_GET['delete'])) {
          try {
            $trajetTypeManager = new TrajetTypeManager($bdd);
            foreach ($_POST['supprimer'] as $trajetASupprimer) {
              $trajetTypeManager->delete($trajetASupprimer);
            }
          }
          catch (Exception $e) {
            $_SESSION['popup_type'] = "error";
            $_SESSION['popup_content'] = "Une erreur est survenue lors de la suppression du trajet type. Veuillez réessayer.";
            header('Location: admin.php?t=tt');
          }
        }
      }

        // Analyse des trajets disponibles
        else if ($_GET['t'] == "tt" && isset($_GET['analyse'])) {
          $trajetTypeManager = new TrajetTypeManager($bdd);
          $resultatsAnalyse = $trajetTypeManager->getResultatsAnalyse();
          $tpl->assign(array(
            'resultatsAnalyse' => $resultatsAnalyse,
            'afficheAnalyse' => true
          ));
        }
        else {
          $tpl->assign(array(
            'afficheAnalyse' => false
          ));
        }

      // Affichage des pages
      if (!empty($_GET['t']) && in_array($_GET['t'], ["users", "tt", "stats"])) {

        switch ($_GET['t']) {

          case "users":
            $membreManager = new MembreManager($bdd);
            $membres = $membreManager->getListe("LIMIT 0,100");
            $tpl->assign(array(
              'membres' => $membres
              ));
            $tpl->display('specific/admin-users.html');
            break;

          case "tt":
            $trajetTypeManager = new TrajetTypeManager($bdd);
            $trajetsTypes = $trajetTypeManager->getListe("LIMIT 0,100");
            $tpl->assign(array(
              'trajetsTypes' => $trajetsTypes
            ));
            $tpl->display('specific/admin-tt.html');
            break;

          case "stats":
            $statistiquesManager = new StatistiquesManager($bdd);
            $statistiques = $statistiquesManager->getStatistiques();
            $tpl->assign(array(
              'nbrVisiteurs' => $statistiques->getValeur('nbrEnLignes'),
              'nbrMembres' => $statistiques->getValeur('nbrMembres'),
              'distanceParcourue' => $statistiques->getValeur('distanceParcourue'),
              'distanceEconomisee' => $statistiques->getValeur('distanceEconomisee'),
              'nbrTT' => $statistiques->getValeur('nbrTrajetType'),
              'nbrTD' => $statistiques->getValeur('nbrTD'),
              'nbrTDDispos' => $statistiques->getValeur('nbrTDDispos'),
              'nbrTDEffectues' => $statistiques->getValeur('nbrTDEffectues')
            ));
            $tpl->display('specific/admin-stats.html');
        }
      }

      // Si on accède à la mauvaise page
      else {
        $_SESSION['popup_type'] = "error";
        $_SESSION['popup_content'] = "Impossible d'accéder à cette page, son adresse est mal formée.";
        header('Location: index.php');
      }
    }

    // Si on est pas administrateur connecté
    else {
      $_SESSION['popup_type'] = "error";
      $_SESSION['popup_content'] = "Vous devez être administrateur pour pouvoir accéder à cette page.";
      header('Location: index.php');
    }

    require_once('includes/footer.php');
