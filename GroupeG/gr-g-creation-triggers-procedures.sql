/*
fichier gr-g-creation-triggers-procedures.sql
21003928, Pérution-Kihli, Guillaume
21509631, Monti, Paul
*/
DELIMITER |

/*--------------------
 Triggers
--------------------*/

/* Statistiques */

-- Nombre de connectés
CREATE TRIGGER afterInsertVisiteur AFTER INSERT
ON Visiteur FOR EACH ROW
BEGIN
  UPDATE Statistiques
  SET valeur = valeur + 1
  WHERE nom = 'nbrEnLignes';
END |

CREATE TRIGGER afterDeleteVisiteur AFTER DELETE
ON Visiteur FOR EACH ROW
BEGIN
  UPDATE Statistiques
  SET valeur = valeur - 1
  WHERE nom = 'nbrEnLignes';
END |

-- Nombre de membres total
CREATE TRIGGER afterInsertMembre AFTER INSERT
ON Membre FOR EACH ROW
BEGIN
  UPDATE Statistiques
  SET valeur = valeur + 1
  WHERE nom = 'nbrMembres';
END |

CREATE TRIGGER afterDeleteMembre AFTER DELETE
ON Membre FOR EACH ROW
BEGIN
  UPDATE Statistiques
  SET valeur = valeur - 1
  WHERE nom = 'nbrMembres';
END |

-- Nombre de trajets types
CREATE TRIGGER afterInsertTrajetType AFTER INSERT
ON TrajetType FOR EACH ROW
BEGIN
  UPDATE Statistiques
  SET valeur = valeur + 1
  WHERE nom = 'nbrTrajetType';
END |
CREATE TRIGGER afterDeleteTrajetType AFTER DELETE
ON TrajetType FOR EACH ROW
BEGIN
  UPDATE Statistiques
  SET valeur = valeur - 1
  WHERE nom = 'nbrTrajetType';
END |

-- Nombre de trajets disponibles et effectués
-- Activé à chaque appel de la procédure mise_a_jour_td()
CREATE TRIGGER afterUpdateTrajetDisponible AFTER UPDATE
ON TrajetDisponible FOR EACH ROW
BEGIN
  UPDATE Statistiques
  SET valeur = valeur + 1
  WHERE nom = 'nbrTDEffectues' AND OLD.effectue = 0 AND NEW.effectue = 1;
  UPDATE Statistiques
  SET valeur = valeur - 1
  WHERE nom = 'nbrTDEffectues' AND OLD.effectue = 1 AND NEW.effectue = 0;
  UPDATE Statistiques
  SET valeur = valeur - 1
  WHERE nom = 'nbrTDDispos' AND OLD.effectue = 0 AND NEW.effectue = 1;
  UPDATE Statistiques
  SET valeur = valeur + 1
  WHERE nom = 'nbrTDDispos' AND OLD.effectue = 1 AND NEW.effectue = 0;
END |

-- Distance parcourue
-- Et nombre de trajets disponibles et effectués - complément
CREATE TRIGGER afterInsertTrajetDisponible AFTER INSERT
ON TrajetDisponible FOR EACH ROW
BEGIN
  UPDATE Statistiques
  SET valeur = valeur + NEW.distance
  WHERE nom = 'distanceParcourue';
  UPDATE Statistiques
  SET valeur = valeur + 1
  WHERE nom = 'nbrTD';
  UPDATE Statistiques
  SET valeur = valeur + 1
  WHERE nom = 'nbrTDDispos' AND NEW.effectue = 0;
  UPDATE Statistiques
  SET valeur = valeur + 1
  WHERE nom = 'nbrTDEffectues' AND NEW.effectue = 1;
END |

CREATE TRIGGER afterDeleteTrajetDisponible AFTER DELETE
ON TrajetDisponible FOR EACH ROW
BEGIN
  UPDATE Statistiques
  SET valeur = valeur - OLD.distance
  WHERE nom = 'distanceParcourue';
  UPDATE Statistiques
  SET valeur = valeur - 1
  WHERE nom = 'nbrTD';
  UPDATE Statistiques
  SET valeur = valeur - 1
  WHERE nom = 'nbrTDDispos' AND OLD.effectue = 0;
  UPDATE Statistiques
  SET valeur = valeur - 1
  WHERE nom = 'nbrTDEffectues' AND OLD.effectue = 1;
END |

-- Distance économisée
CREATE TRIGGER afterInsertParticipe AFTER INSERT
ON Participe FOR EACH ROW
BEGIN
  UPDATE Statistiques
  SET valeur = valeur + (SELECT distance FROM TrajetDisponible WHERE idTrajetDisponible = NEW.idParticipation)
  WHERE nom = 'distanceEconomisee';
END |

CREATE TRIGGER afterDeleteParticipe AFTER DELETE
ON Participe FOR EACH ROW
BEGIN
  UPDATE Statistiques
  SET valeur = valeur - (SELECT distance FROM TrajetDisponible WHERE idTrajetDisponible = OLD.idParticipation)
  WHERE nom = 'distanceEconomisee';
END |

/* Autres */

-- Mise à jour de la note moyenne d'utilisateur
CREATE TRIGGER afterInsertAppreciation AFTER INSERT
ON Appreciation FOR EACH ROW
BEGIN
    DECLARE TOTAL INTEGER;
    DECLARE ID_CONDUCTEUR INTEGER;
    DECLARE NB_NOTES INTEGER;
    SELECT noteMoyenne * nbrNotes, idMembre, nbrNotes INTO @TOTAL, @ID_CONDUCTEUR, @NB_NOTES FROM Membre WHERE NEW.idTrajet = idTrajetDisponible AND conducteur = idMembre;
  UPDATE Membre
  SET noteMoyenne =
    (
      @TOTAL +
      ( NEW.note )
    ) / (
      1 + @NB_NOTES
    )
  WHERE @ID_CONDUCTEUR = idMembre;
END |

-- Interdire la participation d'un conducteur à son trajet comme passager
CREATE TRIGGER beforeInsertParticipe BEFORE INSERT
ON Participe FOR EACH ROW
BEGIN
  DECLARE A INTEGER;
  CASE NEW.idParticipant
    WHEN  (SELECT conducteur FROM TrajetDisponible WHERE idTrajetDisponible = NEW.idParticipation)
    THEN SELECT 1, 2 INTO @A;
  END CASE;
END |

-- Vérifier l'ordre des étapes d'un trajet
CREATE TRIGGER beforeInsertPassePar BEFORE INSERT
ON PassePar FOR EACH ROW
BEGIN
  DECLARE A INTEGER;
  IF EXISTS (SELECT idTrajet FROM PassePar WHERE idTrajet = NEW.idTrajet AND ordre = NEW.ordre)
    THEN (SELECT 1, 2 INTO @A);
  END IF;
END |

/*--------------------
 Procédures
--------------------*/

-- Changer l'état d'un trajet en effectué une fois sa date passée
-- Appelée comme tâche CRON toutes les 10 minutes
CREATE PROCEDURE mise_a_jour_td()
BEGIN
  UPDATE TrajetDisponible
  SET effectue = 1
  WHERE effectue = 0 AND dateTrajet + INTERVAL dureeEstimee MINUTE < NOW();
END |

-- Changer les droits d'un utilisateur
CREATE PROCEDURE changer_admin(IN pIdMembre INT, IN pRole BOOLEAN)
BEGIN
  UPDATE Membre SET role = 'Administrateur' WHERE idMembre = pIdMembre AND pRole = 1;
  UPDATE Membre SET role = 'Membre' WHERE idMembre = pIdMembre AND pRole = 0;
END |

-- Analyser les trajets disponibles et retourne le nombre de trajet par couple de villes
CREATE PROCEDURE analyse_td()
BEGIN
  SELECT d.ville As ville_depart, a.ville AS ville_arrivee, COUNT(*) AS compte FROM TrajetDisponible, PassePar ppd, PassePar ppa, Adresse d, Adresse a
  WHERE
    idTrajetDisponible = ppd.idTrajet AND idTrajetDisponible = ppa.idTrajet AND
    d.idAdresse = ppd.adresse AND a.idAdresse = ppa.adresse AND
    ppd.ordre = (SELECT ordre FROM PassePar pt WHERE pt.idTrajet = ppd.idTrajet ORDER BY ordre ASC LIMIT 0, 1) AND
    ppa.ordre = (SELECT ordre FROM PassePar pt WHERE pt.idTrajet = ppa.idTrajet ORDER BY ordre DESC LIMIT 0, 1)
  GROUP BY d.ville, a.ville;
END |

-- Transformer un trajet type en trajet disponible
CREATE PROCEDURE td_vers_tt(IN pIdTd INT, IN pDistance INT, IN pTemps INT, IN pPrix INT)
BEGIN
  INSERT INTO TrajetType (villeDepart, villeArrivee, distance, tempsIndicatif, plafondPrixKm)
  VALUES (  (SELECT ville FROM TrajetDisponible, PassePar, Adresse WHERE idTrajetDisponible = idTrajet AND adresse = idAdresse ORDER BY ordre ASC LIMIT 0, 1),
            (SELECT ville FROM TrajetDisponible, PassePar, Adresse WHERE idTrajetDisponible = idTrajet AND adresse = idAdresse ORDER BY ordre DESC LIMIT 0, 1),
            pDistance,
            pTemps,
            pPrix);
END |
