/*
fichier gr-g-creation-structure.sql
21003928, Pérution-Kihli, Guillaume
21509631, Monti, Paul
*/

/*--------------------
 Création de la base de donnée
--------------------*/

CREATE TABLE Adresse (
    idAdresse           INTEGER         AUTO_INCREMENT,
    numeroVoie          VARCHAR(10),
    nomVoie             VARCHAR(200),
    complement          VARCHAR(500),
    CP                  VARCHAR(10),
    ville               VARCHAR(70)     NOT NULL,
    CONSTRAINT Adresse_PK PRIMARY KEY (idAdresse),
    INDEX(idAdresse)
);

CREATE TABLE Membre (
    idMembre            INTEGER         AUTO_INCREMENT,
    mail                VARCHAR(50)     NOT NULL,
    mdp                 VARCHAR(128)    NOT NULL,
    role                VARCHAR(14)     NOT NULL,
    nom                 VARCHAR(50)     NOT NULL,
    prenom              VARCHAR(50)     NOT NULL,
    dateNaissance       DATE            NOT NULL,
    habite              INTEGER,
    telephone           VARCHAR(11),
    noteMoyenne         FLOAT,
    nbrNotes            INTEGER         DEFAULT 0,
    dateInscription     DATE            NOT NULL,
    derniereConnexion   DATETIME,
    banni               DATETIME,
    CONSTRAINT roleList CHECK(role IN ('Membre', 'Administrateur')),
    CONSTRAINT Membre_FK FOREIGN KEY (habite) REFERENCES Adresse(idAdresse) ON DELETE SET NULL,
    CONSTRAINT Membre_PK PRIMARY KEY (idMembre),
    INDEX(idMembre)
);

CREATE TABLE Vehicule (
    idVehicule          INTEGER         AUTO_INCREMENT,
    idPossesseur        INTEGER         NULL,
    marque              VARCHAR(50)     NOT NULL,
    modele              VARCHAR(50)     NOT NULL,
    annee               INTEGER,
    consommation        FLOAT,
    nbrPlacesPassagers  INTEGER         NOT NULL,
    CONSTRAINT nombrePlacesMini CHECK (nombrePlaces > 0),
    CONSTRAINT Vehicule_FK FOREIGN KEY (idPossesseur) REFERENCES Membre(idMembre) ON DELETE SET NULL,
    CONSTRAINT Vehicule_PK PRIMARY KEY (idVehicule),
    INDEX(idVehicule)
);

CREATE TABLE TrajetType (
    idTrajetType        INTEGER         AUTO_INCREMENT,
    villeDepart         VARCHAR(70)     NOT NULL,
    villeArrivee        VARCHAR(70)     NOT NULL,
    distance            INTEGER         NOT NULL,
    tempsIndicatif      INTEGER         NOT NULL,
    plafondPrixKm       FLOAT           NOT NULL,
    CONSTRAINT TrajetType_PK PRIMARY KEY (idTrajetType),
    INDEX(idTrajetType)
);

CREATE TABLE TrajetDisponible (
    idTrajetDisponible  INTEGER         AUTO_INCREMENT,
    conducteur          INTEGER         NOT NULL,
    distance            INTEGER         NOT NULL,
    dateTrajet          DATETIME        NOT NULL,
    dureeEstimee        INTEGER         NOT NULL,
    prixKm              FLOAT           NOT NULL,
    description         TEXT,
    cigaretteAutorisee  BOOLEAN         NOT NULL,
    bagagesAutorises    VARCHAR(5)    NOT NULL,
    animalAutorise      BOOLEAN         NOT NULL,
    dateCreation        DATETIME        NOT NULL,
    vehicule            INTEGER         NOT NULL,
    implemente          INTEGER         NULL,
    effectue            BOOLEAN         DEFAULT 0,
    CONSTRAINT limiteBagage CHECK (bagagesAutorises IN ('SANS', 'TOUT', 'PETIT', 'MOYEN', 'GROS')),
    CONSTRAINT TrajetDisponible_FK1 FOREIGN KEY (conducteur) REFERENCES Membre(idMembre) ON DELETE CASCADE,
    CONSTRAINT TrajetDisponible_FK2 FOREIGN KEY (vehicule) REFERENCES Vehicule(idVehicule) ON DELETE CASCADE,
    CONSTRAINT TrajetDisponible_FK3 FOREIGN KEY (implemente) REFERENCES TrajetType(idTrajetType) ON DELETE SET NULL,
    CONSTRAINT TrajetDisponible_PK PRIMARY KEY (idTrajetDisponible),
    INDEX(idTrajetDisponible)
);

CREATE TABLE Participe (
    idParticipant       INTEGER         NOT NULL,
    idParticipation     INTEGER         NOT NULL,
    CONSTRAINT Participe_FK1 FOREIGN KEY (idParticipant) REFERENCES Membre(idMembre) ON DELETE CASCADE,
    CONSTRAINT Participe_FK2 FOREIGN KEY (idParticipation) REFERENCES TrajetDisponible(idTrajetDisponible) ON DELETE CASCADE
);

CREATE TABLE PassePar (
    idTrajet            INTEGER         NOT NULL,
    adresse             INTEGER         NOT NULL,
    ordre               INTEGER         NOT NULL,
    CONSTRAINT PassePar_FK1 FOREIGN KEY (idTrajet) REFERENCES TrajetDisponible(idTrajetDisponible) ON DELETE CASCADE,
    CONSTRAINT PassePar_FK2 FOREIGN KEY (adresse) REFERENCES Adresse(idAdresse) ON DELETE CASCADE
);


CREATE TABLE Message (
    idMessage           INTEGER         AUTO_INCREMENT,
    corps               TEXT            NOT NULL,
    idTrajet            INTEGER         NOT NULL,
    idAuteur            INTEGER         NOT NULL,
    CONSTRAINT Message_FK1 FOREIGN KEY (idAuteur) REFERENCES Membre(idMembre) ON DELETE CASCADE,
    CONSTRAINT Message_FK2 FOREIGN KEY (idTrajet) REFERENCES TrajetDisponible(idTrajetDisponible) ON DELETE CASCADE,
    CONSTRAINT Message_PK PRIMARY KEY (idMessage)
);

CREATE TABLE Appreciation (
    idAppreciation      INTEGER         AUTO_INCREMENT,
    corps               TEXT,
    note                INTEGER         NOT NULL,
    idAuteur            INTEGER         NOT NULL,
    idTrajet            INTEGER         NOT NULL,
    CONSTRAINT intervalleNotre CHECK (note >= 0 AND note <= 10),
    CONSTRAINT Appreciation_FK1 FOREIGN KEY (idAuteur) REFERENCES Membre(idMembre) ON DELETE CASCADE,
    CONSTRAINT Appreciation_FK2 FOREIGN KEY (idTrajet) REFERENCES TrajetDisponible(idTrajetDisponible) ON DELETE CASCADE,
    CONSTRAINT Appreciation_PK PRIMARY KEY(idAppreciation)
);

CREATE TABLE Visiteur (
    idSession           VARCHAR(40),
    IP                  VARCHAR(39)     NOT NULL,
    navigateur          VARCHAR(200),
    dateDernierePageVue TIMESTAMP,
    dernierePageVue     VARCHAR(200),
    idMembreConnecte    INTEGER         NULL,
    CONSTRAINT Visiteur_FK FOREIGN KEY(idMembreConnecte) REFERENCES Membre(idMembre) ON DELETE CASCADE,
    CONSTRAINT Visiteur_PK PRIMARY KEY (idSession)
);

CREATE TABLE Statistiques (
  nom                   VARCHAR(50),
  valeur                FLOAT,
  CONSTRAINT Statistiques_PK PRIMARY KEY (nom)
);

INSERT INTO Statistiques (nom, valeur) VALUES
  ("nbrEnLignes", 0),
  ("nbrMembres", 0),
  ("distanceParcourue", 0),
  ("distanceEconomisee", 0),
  ("nbrTrajetType", 0),
  ("nbrTD", 0),
  ("nbrTDDispos", 0),
  ("nbrTDEffectues", 0);
