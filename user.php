<?php

    define('Vitoco', true);
    $titre = "Détails de l'utilisateur";
    $cache = false;

    require_once('includes/init.php');
    require_once('includes/head.php');
    require_once('includes/popup.php');

    $tpl = new Smarty;

    // Affichage de la page d'un utilisateur
    if (!empty($_GET['u'])) {
      $membreManager = new MembreManager($bdd);
      $appreciationManager = new AppreciationManager($bdd);
      try {
        $appreciations = $appreciationManager->getListe("", ['conducteur', DB::EGAL, $_GET['u']]);
        $membre = $membreManager->getMembre(['idMembre', DB::EGAL, $_GET['u']]);
        $membreManager->loadAdresses([$membre]);
        $membreManager->loadVehicules([$membre]);

        $tpl->assign(array(
          'nom' => $membre->getNom(),
          'prenom' => $membre->getPrenom(),
          'age' => $membre->getAge(),
          'ville' => $membre->getAdresse()->getVille(),
          'noteMoyenne' => $membre->getNoteMoyenne(),
          'nombreEvaluations' => $membre->getNbrNotes(),
          'dateInscription' => $membre->getDateInscription("d/m/Y"),
          'nombreDeVehicules' => count($membre->getVehicules()),
          'vehicules' => $membre->getVehicules(),
          'nombreEvaluations' => count($appreciations)
      		));
      }
      catch (Exception $e) {
        $_SESSION['popup_type'] = "error";
    	  $_SESSION['popup_content'] = "Une erreur est survenue lors du chargement des informations de cet utilisateur.<br>".$e->getMessage();
        header('Location: index.php');
      }
    }

    // Si on accède à une page d'utilisateur inconnu
    else {
      $_SESSION['popup_type'] = "error";
  	  $_SESSION['popup_content'] = "La page utilisateur à laquelle vous essayez d'accéder n'existe pas.<br>".$e->getMessage();
      header('Location: index.php');
    }

    $tpl->display('specific/user.html');

    require_once('includes/footer.php');
